## VAN Sample App

This is a sample app that interacts with the [NGP VAN API](https://developers.ngpvan.com/van-api) and *mostly* resembles the look of [ProgressUI](https://progress.ngpvan.com/) and the [VAN website](https://demo.mainvan.us).

#### Source Code

[https://github.com/sschottler/VANSampleApp](https://github.com/sschottler/VANSampleApp)

#### Website URL

[https://vansampleapp.azurewebsites.net/](https://vansampleapp.azurewebsites.net/)

### Requesting your own VAN API key

The azure site uses my test environment VAN API credentials, which aren't checked into github, so you will need your own to call the VAN API. The app will still run if you leave the settings blank (`startup.cs` will detect the lack of configuration and wire up fake data services), but if you want to get your own API key, follow these steps:

1. Request your VAN API credentials [here](https://developers.ngpvan.com/apiKey/request). 
2. Once you receive your credentials, update the `VANSettings.ApplicationName` and `VANSettings.APIKey` settings in appsettings.json or your secrets.json file (right click project in Visual Studio -> Manage User Secrets) and rerun the app.

### Prerequisites

1. [npm](https://www.npmjs.com/get-npm)
2. OPTIONAL:
   1. [.NET Core 2.1](https://www.microsoft.com/net/learn/get-started-with-dotnet-tutorial) if you want to run the Web API backend
   2. [Visual Studio 2017 or Visual Studio Code](https://visualstudio.microsoft.com/downloads/)

### Running the App

1. `Git clone https://github.com/sschottler/VANSampleApp.git` or download & extract the zip file from [github](https://github.com/sschottler/VANSampleApp)
2. Open command prompt and `cd` to ClientApp directory. Run `npm install`
3. Open the project in Visual Studio or VS Code and start debugging 
   * *`startup.cs` will automatically run the `npm start` script and if no VAN configuration is detected in appsettings.json or secrets.json, it will wire up fake data services so the app still runs*
   * *if you don't want to use Visual Studio, you could also run `dotnet run` from a command prompt in the directory with `VANSampleApp.csproj`*
   
### Running the App without .NET 

All VAN API calls go through JavaScript service classes that are injected via props by the `withServices.jsx` HOC. This is why many components have two exports - one for tests to import mocks via props and a default export for regular UI. There are fake implementations of all these services in the `services/fake` folder that do not call the API and simply return hard-coded JavaScript data. This makes it easy to hard-code data and test different scenarios without having to change C# code, recompile, and rerun. To use the fake services:

1. Change the imports in `ClientApp/src/components/common/withServices.jsx` from:

  ```js
  import services from '../../services'
  //import services from '../../services/fake'
  ```

  to this:

  ```js
  //import services from '../../services'
  import services from '../../services/fake'
  ```

2. Open a command prompt in the ClientApp directory and run `npm start`

### Browser Support

Tested on newer versions of Chrome and Edge on Windows 10 and on my Samsung Galaxy S8+. Most of the ES6 gets transpiled, but I didn't take the time to add polyfills for things like includes methods on arrays, so it won't work on older browsers, but you will get to see the `<ErrorBoundary>` component in action! 

### Technology Used

* [VAN API](https://developers.ngpvan.com/van-api)
* [ProgressUI](https://progress.ngpvan.com/)...sort of
  * I am not a member of NGPVAN org, so I couldn't access the react component library ([https://progress-ui-react.azurewebsites.net/](https://progress-ui-react.azurewebsites.net/))
  * I also had some issues with bringing in the ProgressUI style sheet - missing assets, etc. so I ended up just writing my own CSS/react components from scratch and tried to make it match ProgressUI and the VAN demo site.
* HTML5/CSS3/[SASS](https://sass-lang.com/) 
  * used [BEM](http://getbem.com/) naming conventions.
  * sidebar should hamburger up on mobile devices < 768px and be responsive
* [React](https://reactjs.org/) 
  * [Create React App](https://github.com/facebook/create-react-app#create-react-app-)
    * [ES6 JavaScript with Babel transpiler](https://babeljs.io/) 
  * [React Bootstrap](https://react-bootstrap.github.io/components/alerts/)
  * [React Router](https://reacttraining.com/react-router/)
  * [React Loadable for code splitting](https://github.com/jamiebuilds/react-loadable)
* [C#/ASP.NET Core 2.1 Web API](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.1)
* [Azure App Services](https://azure.microsoft.com/en-us/services/app-service/)
* [Azure Search](https://azure.microsoft.com/en-us/services/search/)
  * based on Elastic Search
  * used in a pretty contrived way on People view that wouldn't make sense for a real app, but VAN API endpoints did not offer a way to retrieve all people created
  * OPTIONAL: to setup your own search index, reference [these instructions](https://docs.microsoft.com/en-us/azure/search/search-query-dotnet) and update your appsettings.json or secrets.json file. 
* [Redis distributed cache](https://azure.microsoft.com/en-us/services/cache/)
  * OPTIONAL: to setup an Azure Redis Cache, reference [these instructions](https://docs.microsoft.com/en-us/azure/redis-cache/cache-dotnet-core-quickstart) and update your appsettings.json or secrets.json file. To install Redis locally, reference [these instructions](https://docs.microsoft.com/en-us/aspnet/core/performance/caching/distributed?view=aspnetcore-2.1#using-a-redis-distributed-cache) to use the chocolatey package
* [Google Places autocomplete](https://developers.google.com/places/web-service/autocomplete) for address lookups on create location and create person views
* [Google Maps integration](https://tomchentw.github.io/react-google-maps/) for any locations that have geo coordinates
* [D3/Recharts data visualization](http://recharts.org)

### Analyze the webpack bundle

1. Open a command prompt and `cd` to the ClientApp folder
2. run `npm run analyze`. This will run the build script with a webpack analyzer plugin added.

### TODO

* fix create person view VAN API request
* integrate Auth0 OpenID JWT login
* optimize bundle more
  * put react-bootstrap-table in common chunk or replace with a better component
* convert to TypeScript and remove prop-types
* Jest/Enzyme tests
* Cancellable promises for isMounted issue ([more info](https://reactjs.org/blog/2015/12/16/ismounted-antipattern.html))
* server-side rendering
* redux 
* refactor some shared container & services logic to recompose HOCs