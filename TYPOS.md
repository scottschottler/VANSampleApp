### Typos I noticed while reading NGP VAN documentation

- ProgressUI docs
  - [Grid](https://progress.ngpvan.com/getting-started/grid)
    - _"This is the preferred basic 12 column grid structure ~~give~~ **that gives/giving** you access"_
  - [Page Templates](https://progress.ngpvan.com/page-templates)
    - _"details ~~fir~~ **for** an event"_
  - [Sidebar](https://progress.ngpvan.com/documentation/sidebar)
    - "glyhpicon" should be **glyphicon** everywhere in the code snippet
    - _"~~swithc~~ **switch** between apps"_
  - [Header](https://progress.ngpvan.com/documentation/header)
    - _"allows users on any ~~devide~~ **device**"_
  - [Tables](https://progress.ngpvan.com/documentation/tables)
    - _"When ~~usinga~~ **using a** table"_
  - [Switches](https://progress.ngpvan.com/documentation/switches)
    - _"within the switch to ~~indentify~~ **identify**"_
  - [Map Marker Popups](https://progress.ngpvan.com/documentation/maps#popups)
    - User Swatches link is broken
- demo.mainvan.us
  - [Main Menu Wiki](https://demo.mainvan.us/wikidetails.aspx?WikiItemTitle=Main%20Menu)
    - "to see how support for ~~you~~ **your** candidate"
