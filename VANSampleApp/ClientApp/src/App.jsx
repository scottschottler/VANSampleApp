import React from 'react'
import { Route, Switch } from 'react-router'

import Layout from './components/layout/Layout'
import ReadMeView from './components/readme/ReadMeView'
import APITestViewContainer from './components/apitest/APITestViewContainer'
import PeopleViewContainer from './components/people/PeopleViewContainer'
import CreatePersonViewContainer from './components/people/CreatePersonViewContainer'
import LocationsViewContainer from './components/locations/LocationsViewContainer'
import LoadableReportsView from './components/reports/LoadableReportsView'
import './VanPage.css'

const App = () => (
	<Layout>
		<Switch>
			<Route exact path='/' component={ReadMeView} />
			<Route exact path='/apitest' component={APITestViewContainer} />
			<Route exact path='/people' component={PeopleViewContainer} />
			<Route path='/people/new' component={CreatePersonViewContainer} />
			<Route path='/locations/:locationId?' component={LocationsViewContainer} />
			<Route path='/reports' component={LoadableReportsView} />
		</Switch>
	</Layout>
)

export default App