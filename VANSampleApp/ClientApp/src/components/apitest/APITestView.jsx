﻿import React from 'react'
import PropTypes from 'prop-types'
import { ControlLabel, FormControl } from 'react-bootstrap'

import FormGroup from '../common/FormGroup'
import LoadableWebResponse from '../common/LoadableWebResponse'
import View from '../layout/View'
import Button from '../common/Button'
import Link from '../common/Link'

const APITestView = ({ input, validate, error, onChange, onSubmit, serviceURL, result }) => {
	const showBody = (input.verb !== 'GET')
	const { errors, isValid } = error

	return (
		<View title="Test REST API" className="api-test">
			<p>
				<Link href="https://developers.ngpvan.com/van-api" target="_blank">
					VAN API docs
				</Link>
			</p>
			
			<form onSubmit={onSubmit}>
				<FormGroup controlId="dbMode">
					<ControlLabel>DB Mode</ControlLabel>
					<FormControl
						componentClass="select"
						placeholder="select"
						onChange={onChange}
						value={input.dbMode}>
						<option value="0">VoterFile</option>
						<option value="1">MyCampaign</option>
					</FormControl>
				</FormGroup>

				<FormGroup controlId="verb">
					<ControlLabel>HTTP Verb</ControlLabel>
					<FormControl
						componentClass="select"
						onChange={onChange}
						value={input.verb}>
						<option value="GET">GET</option>
						<option value="POST">POST</option>
						<option value="PUT">PUT</option>
						<option value="DELETE">DELETE</option>
					</FormControl>
				</FormGroup>

				<FormGroup
					controlId="endpoint"
					validationState={validate.endpoint && errors.endpoint && 'error'}>
					<ControlLabel>{serviceURL}</ControlLabel>
					<FormControl
						type="text"
						value={input.endpoint}
						onChange={onChange}
						placeholder="Enter endpoint (relative)" />
					<FormControl.Feedback />
				</FormGroup>

				{showBody &&
					<FormGroup
						controlId="json"
						validationState={validate.json && ((errors.json && 'error') || 'success')}>
						<ControlLabel>JSON Body</ControlLabel>
						<FormControl
							rows={5}
							componentClass="textarea"
							value={input.json}
							onChange={onChange}
							placeholder="Enter JSON" />
						<FormControl.Feedback />
					</FormGroup>
				}

				<FormGroup>
					<Button disabled={!isValid} bsStyle="primary" type="submit">Send</Button>
				</FormGroup>
			</form>

			{result && <LoadableWebResponse response={result} />}
		</View>
	)
}

APITestView.propTypes = {
	onChange: PropTypes.func.isRequired,
	onSubmit: PropTypes.func.isRequired,
	input: PropTypes.object.isRequired,
	validate: PropTypes.object,
	error: PropTypes.object,
	result: PropTypes.object
}

APITestView.defaultProps = {
	error: {},
	validate: {},
	result: null
}

export default APITestView