﻿import React from 'react'
import urlJoin from 'url-join'
import validator from 'validator'

import APITestView from './APITestView'
import withServices from '../common/withServices'

export class APITestViewContainer extends React.Component {
	constructor(props) {
		super(props)
		this.serviceURL = 'https://api.securevan.com/v4/'
		this.api = props.services.api
		
		this.state = {
			input: {
				verb: 'POST',
				endpoint: 'echoes',
				dbMode: 1,
				json: JSON.stringify({ message: 'Hello, world' })
			},
			validate: {},
			result: null
		}
	}

	handleChange = (event) => {
		const fieldName = event.target.id || event.target.name
		const fieldVal = event.target.value

		this.setState(prevState => ({
			input: {
				...prevState.input,
				[fieldName]: fieldVal
			},
			validate: {
				...prevState.validate,
				[fieldName]: true
			}
		}))
	}

	handleSubmit = (event) => {
		event.preventDefault()

		this.api.post('van', this.state.input, response => {
			const { status, data } = response

			this.setState({
				result: { status, data }
			})
		}, false)
	}

	validate() {
		const errors = {}
		const { endpoint, verb, json } = this.state.input

		if (validator.isEmpty(endpoint)) {
			errors.endpoint = 'Endpoint is required'
		} else if (!validator.isURL(urlJoin(this.serviceURL, endpoint))) {
			errors.endpoint = 'Endpoint is not a valid url'
		}
		
		if ((verb === 'POST' || verb === 'PUT') && validator.isEmpty(json)) {
			errors.json = 'Body is required'
		}

		if (verb !== 'GET' && !validator.isEmpty(json) && !validator.isJSON(json)) {
			errors.json = 'Body has invalid JSON'
		}
		
		return {
			errors,
			isValid: Object.keys(errors).length === 0
		}
	}

	render() {
		const { ...propsToPass } = this.state
		const errors = this.validate()

		return (
			<APITestView
				serviceURL={this.serviceURL}
				onChange={this.handleChange}
				onSubmit={this.handleSubmit}
				error={errors}
				{...propsToPass} />
		)
	}
}

export default withServices(APITestViewContainer)