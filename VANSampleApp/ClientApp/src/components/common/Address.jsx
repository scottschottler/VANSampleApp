﻿import React from 'react'
import { ControlLabel, FormControl } from 'react-bootstrap'
import PropTypes from 'prop-types'

import './Address.css'
import Button from './Button'
import LoadableRegionSelect from './LoadableRegionSelect'
import FormGroup from './FormGroup'

class Address extends React.Component {
	handleStateChange = (stateOrProvince) => {
		this.props.onChange({ target: { id: 'stateOrProvince', value: stateOrProvince } })
	}

	handleKeyPress(e) {
		// prevent hitting enter to select google place suggestion from submitting parent form:
		if (e.key === 'Enter')
			e.preventDefault()
	}

	render() {
		const errors = this.props.errors || {}
		const { searchId, input, validate, showName, showClear, clearText, onClear, onChange } = this.props
		const { name, addressLine1, addressLine2, city, stateOrProvince, zipOrPostalCode } = input
		
		return (
			<div className="address">
				<FormGroup controlId={searchId}>
					<ControlLabel>Enter Location</ControlLabel>
					<FormControl
						id={searchId}
						type="text"
						onKeyPress={this.handleKeyPress}
						placeholder="Search google for location" />
				</FormGroup>
				{showName &&
					<FormGroup
						controlId="name"
						validationState={validate.name && errors.name && 'error'}>
						<ControlLabel>Name</ControlLabel>
						<FormControl
							type="text"
							value={name}
							onChange={onChange}
							placeholder="Name"
							maxLength="50" />
					</FormGroup>				
				}
				<FormGroup
					controlId="addressLine1"
					validationState={validate.addressLine1 && errors.addressLine1 && 'error'}>
					<ControlLabel>Address Line 1</ControlLabel>
					<FormControl
						type="text"
						value={addressLine1}
						onChange={onChange}
						placeholder="Address Line 1" />
				</FormGroup>
				<FormGroup controlId="addressLine2">
					<ControlLabel>Address Line 2</ControlLabel>
					<FormControl
						type="text"
						value={addressLine2}
						onChange={onChange}
						placeholder="Address Line 2" />
				</FormGroup>
				<FormGroup
					controlId="city"
					validationState={validate.city && errors.city && 'error'}>
					<ControlLabel>City</ControlLabel>
					<FormControl
						type="text"
						value={city}
						onChange={onChange}
						placeholder="City" />
				</FormGroup>
				<FormGroup
					controlId="state"
					validationState={validate.stateOrProvince && errors.stateOrProvince && 'error'}>
					<ControlLabel>State</ControlLabel>
					<LoadableRegionSelect
						state={stateOrProvince}
						onChange={this.handleStateChange} />
				</FormGroup>
				<FormGroup controlId="zipOrPostalCode">
					<ControlLabel>Zip Code</ControlLabel>
					<FormControl
						type="text"
						value={zipOrPostalCode}
						onChange={onChange} placeholder="Zipcode" />
				</FormGroup>
				{showClear &&
					<div className="text-center">
						<Button onClick={onClear} bsSize="sm">{clearText}</Button>
					</div>
				}
			</div>
		)
	}
}

Address.propTypes = {
	searchId: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
	onClear: PropTypes.func.isRequired,
	input: PropTypes.object.isRequired,
	validate: PropTypes.object, 
	errors: PropTypes.object,
	showName: PropTypes.bool,
	showClear: PropTypes.bool,
	clearText: PropTypes.string
}

Address.defaultProps = {
	showName: true,
	showClear: true,
	clearText: 'Clear',
	errors: {},
	validate: {}
}

export default Address