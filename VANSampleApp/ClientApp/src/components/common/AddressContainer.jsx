﻿import React from 'react'
import parseGooglePlace from 'parse-google-place'
import PropTypes from 'prop-types'
import validator from 'validator'

import Address from './Address'

class AddressContainer extends React.Component {
	constructor(props) {
		super(props)
		this.state = this.initialState()
	}

	initialState() {
		return {
			input: {
				name: '',
				addressLine1: '',
				addressLine2: '',
				city: '',
				stateOrProvince: '',
				zipOrPostalCode: ''
			},
			validate: {}
		}
	}

	searchId = 'google-location-search'
	autocomplete = null

	componentDidMount() {
		// this depends on google places script to be loaded in index.html:
		const google = window.google

		this.autocomplete = new google.maps.places.Autocomplete(document.getElementById(this.searchId), {})
		this.autocomplete.addListener('place_changed', this.handlePlaceSelect)
	}

	handleChange = (event) => {
		const fieldName = event.target.id || event.target.name
		const fieldValue = event.target.value
		
		this.setState(prevState => ({
			input: {
				...prevState.input,
				[fieldName]: fieldValue
			},
			validate: {
				...prevState.validate,
				[fieldName]: true
			}
		}), this.onAddressChanged)
	}

	handleClear = () => {
		this.setState(this.initialState(), () => this.onAddressChanged())
		document.getElementById(this.searchId).value = ''
	}

	handlePlaceSelect = () => {
		const googlePlace = this.autocomplete.getPlace()
		const address = parseGooglePlace(googlePlace)
		const name = (googlePlace.name !== address.address) ? googlePlace.name : ''

		this.setState({
			input: {
				name,
				addressLine1: address.address,
				addressLine2: '',
				city: address.city,
				stateOrProvince: address.stateShort,
				zipOrPostalCode: address.zipCode
			},
			validate: this.initialState().validate
		}, this.onAddressChanged)
	}

	onAddressChanged = () => {
		const { isValid } = this.validate()
		this.props.onAddressChanged(this.state.input, isValid)
	}

	validate() {
		const errors = {}
		const { showName } = this.props
		const { name, addressLine1, city, stateOrProvince } = this.state.input

		if (showName && validator.isEmpty(name))
			errors.name = 'Name is required'

		if (validator.isEmpty(addressLine1))
			errors.addressLine1 = 'Address line 1 is required'

		if (validator.isEmpty(city))
			errors.city = 'City is required'

		if (validator.isEmpty(stateOrProvince))
			errors.stateOrProvince = 'State is required'
		
		return {
			errors,
			isValid: Object.keys(errors).length === 0
		}
	}

	render() {
		const { validate, ...propsToPass } = this.props
		const errors = validate ? this.validate().errors : null
		const { ...stateToPass } = this.state

		return (
			<Address
				searchId={this.searchId}
				onChange={this.handleChange}
				onClear={this.handleClear}
				errors={errors}
				{...propsToPass}
				{...stateToPass} />
		)
	}
}

AddressContainer.propTypes = {
	onAddressChanged: PropTypes.func.isRequired,
	showName: PropTypes.bool,
	showClear: PropTypes.bool,
	clearText: PropTypes.string,
	validate: PropTypes.bool
}

AddressContainer.defaultProps = {
	showName: true,
	showClear: true,
	clearText: 'Clear',
	validate: true
}

export default AddressContainer