import React from 'react'
import { Button as BootstrapButton } from 'react-bootstrap'

import './Button.css'

const Button = props => <BootstrapButton {...props} />

Button.propTypes = BootstrapButton.propTypes

export default Button