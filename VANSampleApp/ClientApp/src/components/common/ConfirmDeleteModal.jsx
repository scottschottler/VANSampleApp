﻿import React from 'react'
import { Modal } from 'react-bootstrap'
import PropTypes from 'prop-types'

import Button from './Button'

const ConfirmDeleteModal = ({ title, message, onClose, onConfirm, show, deleteText }) => (
	<Modal show={show} onHide={onClose}>
		<Modal.Header>
			<Modal.Title>{title}</Modal.Title>
		</Modal.Header>

		<Modal.Body>{message}</Modal.Body>

		<Modal.Footer>
			<Button onClick={onClose}>Cancel</Button>
			<Button bsStyle="danger" onClick={onConfirm}>{deleteText}</Button>
		</Modal.Footer>
	</Modal>
)

ConfirmDeleteModal.propTypes = {
	onClose: PropTypes.func.isRequired,
	onConfirm: PropTypes.func.isRequired,
	title: PropTypes.string,
	message: PropTypes.string,
	deleteText: PropTypes.string
}

ConfirmDeleteModal.defaultProps = {
	title: 'Confirm Delete',
	message: 'Are you sure you want to delete?',
	deleteText: 'DIE!'
}

export default ConfirmDeleteModal