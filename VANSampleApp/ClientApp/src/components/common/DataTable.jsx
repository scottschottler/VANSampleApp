﻿import React from 'react'
import { Row, Col, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { BootstrapTable, InsertButton, ExportCSVButton, DeleteButton } from 'react-bootstrap-table'
import '../../../node_modules/react-bootstrap-table/dist/react-bootstrap-table-all.min.css'

import ConfirmDeleteModal from './ConfirmDeleteModal'
import SearchInput from './SearchInput'
import './DataTable.css'

class DataTable extends React.Component {
	state = {
		showConfirm: false,
		onConfirmAction: null
	}

	renderNewButton = (onClick) => {
		const { newButtonText, onNewClick } = this.props.options

		return (
			<InsertButton
				btnContextual="btn-primary"
				btnText={newButtonText}
				onClick={onNewClick} />
		)
	}

	renderExportButton = (onClick) => (
		<OverlayTrigger
			placement="top"
			delayShow={400}
			overlay={<Tooltip id="excel">Export to CSV</Tooltip>}>
			<ExportCSVButton
				btnGlyphicon='glyphicon-download-alt'
				btnText="Export"
				onClick={onClick} />
		</OverlayTrigger>
	)

	renderDeleteButton = (onClick) => (
		<OverlayTrigger
			placement="top"
			delayShow={400}
			overlay={<Tooltip id="delete">Delete selected row(s)</Tooltip>}>
			<DeleteButton btnContextual="btn-danger" onClick={onClick} />
		</OverlayTrigger>
	)

	renderToolbar = (props) => {
		const {
			onSearch,
			onSearchChange,
			searchPlaceholder,
			searchTooltip,
			searchValue
		} = this.props.options

		return (
			<div className="toolbar" style={{ marginLeft: 15, marginRight: 15 }}>
				<Row>
					<Col md={6}>
						<div className="btn-group btn-group-sm toolbar-buttons" role="group">
							{props.components.insertBtn}
							{props.components.exportCSVBtn}
							{props.components.deleteBtn}
						</div>
					</Col>
					<Col md={6}>
						<SearchInput
							onSearch={onSearch}
							onChange={onSearchChange}
							tooltip={searchTooltip}
							placeholder={searchPlaceholder}
							value={searchValue} />
					</Col>
				</Row>
			</div>
		)
	}

	showCustomConfirm = (next, keys) => {
		this.setState({ showConfirm: true, onConfirmAction: next })
	}

	handleConfirmClose = () => {
		this.setState({ showConfirm: false })
	}

	handleConfirmDelete = () => {
		this.state.onConfirmAction()
		this.setState({ showConfirm: false })
	}

	remote(remoteConfig) {
		remoteConfig.search = true
		remoteConfig.pagination = true
		remoteConfig.dropRow = true

		return remoteConfig
	}

	render() {
		const { options, ...bootstrapTableProps } = this.props
		const { showConfirm } = this.state
		
		const mergedOptions = {
			insertBtn: this.renderNewButton,
			exportCSVBtn: this.renderExportButton,
			deleteBtn: this.renderDeleteButton,
			toolBar: this.renderToolbar,
			// if this is set to empty, it will show default message which I don't want during loading
			noDataText: ' ',
			onDeleteRow: options.onDeleteRow,
			onRowClick: options.onRowClick,
			onPageChange: options.onPageChange,
			sizePerPage: options.sizePerPage,
			page: options.page,
			handleConfirmDeleteRow: this.showCustomConfirm,
			sizePerPageList: [
				{ text: '1 (testing)', value: 1 },
				{ text: '2 (testing)', value: 2 },
				{ text: '10', value: 10 },
				{ text: '20', value: 20 },
				{ text: '50', value: 50 },
				{ text: '200', value: 200 }
			]
		}
		
		if (options.isSelectable) {
			bootstrapTableProps.selectRow = {
				mode: 'checkbox',
				className: 'data-table__row--selected'
			}
		}

		if (options.totalDataSize) {
			bootstrapTableProps.fetchInfo = {
				dataTotalSize: options.totalDataSize
			}
		}

		return (
			<React.Fragment>

				<ConfirmDeleteModal
					show={showConfirm}
					message="Are you sure you want to delete the selected row(s)?"
					onClose={this.handleConfirmClose}
					onConfirm={this.handleConfirmDelete} />

				<BootstrapTable
					containerClass="data-table"
					trClassName="data-table__row"
					hover
					remote={this.remote}
					bordered={false}
					insertRow
					exportCSV
					options={mergedOptions}
					{...bootstrapTableProps} />

			</React.Fragment>
		)
	}
}

DataTable.propTypes = BootstrapTable.propTypes

export default DataTable