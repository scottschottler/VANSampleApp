﻿import React from 'react'
import PropTypes from 'prop-types'
import emailMask from 'text-mask-addons/dist/emailMask'
import MaskedInput from 'react-text-mask'

const EmailInput = ({value, onChange}) => (
	<MaskedInput
		id="email"
		name="email"
		value={value}
		onChange={onChange}
		className="form-control"
		placeholder="Email"
		mask={emailMask} />
)

EmailInput.propTypes = {
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired
}

export default EmailInput