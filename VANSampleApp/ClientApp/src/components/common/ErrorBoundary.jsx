﻿import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'react-bootstrap'

class ErrorBoundary extends React.Component {
	state = { hasError: false }
	
	componentDidCatch(error, info) {
		this.setState({ hasError: true })
	}

	componentDidUpdate(prevProps) {
		if (prevProps.children !== this.props.children) 
			this.setState({ hasError: false })
	}

	render() {
		if (this.state.hasError) {
			// this will only show briefly in dev mode before the regular error page appears
			// if you run the build version, however, it works as expected:
			return <Alert bsStyle="danger">{this.props.errorMessage}</Alert>
		}

		return this.props.children
	}
}

ErrorBoundary.propTypes = {
	errorMessage: PropTypes.string
}

ErrorBoundary.defaultProps = {
	errorMessage: 'An error occurred while rendering this view.' 
}

export default ErrorBoundary