﻿import React from 'react'
import { FormGroup as BootstrapFormGroup } from 'react-bootstrap'

import './FormGroup.css'

const FormGroup = (props) => (
	<BootstrapFormGroup {...props} />	
)

FormGroup.propTypes = BootstrapFormGroup.propTypes

export default FormGroup