﻿import React from 'react'

import './FormGroupTitle.css'

const FormGroupTitle = ({ children }) => (
	<legend className="form-group-title">{children}</legend>
)

export default FormGroupTitle