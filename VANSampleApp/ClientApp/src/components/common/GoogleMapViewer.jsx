﻿import React from 'react'
import PropTypes from 'prop-types'
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps'

const GoogleMapViewer = ({ lat, lon, height }) => {
	// weird api and terrible documentation:
	const Map = withGoogleMap((props) =>
		<GoogleMap
			defaultZoom={17}
			defaultCenter={{ lat, lng: lon }}>
			{props.isMarkerShown && <Marker position={{ lat: lat, lng: lon }} />}
		</GoogleMap>
	)
	
	return (
		<Map
			isMarkerShown
			containerElement={<div style={{ height }} />}
			mapElement={<div style={{ height }} />} />
	)
}

GoogleMapViewer.propTypes = {
	lat: PropTypes.number.isRequired,
	lon: PropTypes.number.isRequired,
	height: PropTypes.number
}

GoogleMapViewer.defaultProps = {
	height: 400
}

export default GoogleMapViewer