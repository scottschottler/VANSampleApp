﻿import React from 'react'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'
import PropTypes from 'prop-types'

import Link from './Link'

const GoogleMapsLink = ({ address }) => (
	<OverlayTrigger
		placement='top'
		overlay={<Tooltip id="googleMaps">Click to open in google maps</Tooltip>}>
		<Link
			href={`https://maps.google.com?q=${encodeURIComponent(address)}`}
			onClick={(e) => e.stopPropagation()}
			target="_blank">
			{address}
		</Link>
	</OverlayTrigger>
)

GoogleMapsLink.propTypes = {
	address: PropTypes.string.isRequired
}

export default GoogleMapsLink