﻿import React from 'react'
import PropTypes from 'prop-types'

import './IFrame.css'

class IFrame extends React.Component {
	componentDidMount() {
		this.updateIFrame()
	}

	componentDidUpdate() {
		this.updateIFrame()
	}

	updateIFrame() {
		const iframe = this.refs.iframe
		const document = iframe.contentDocument
		document.body.innerHTML = this.props.content
	}

	render() {
		return <iframe ref="iframe" className="iframe" title="Response" />
	}
}

IFrame.propTypes = {
	content: PropTypes.string
}

export default IFrame