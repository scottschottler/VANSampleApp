﻿import React from 'react'
import classNames from 'classnames'

import './Link.css'

const Link = ({ className, children, ...propsToPass }) => {
	const isExternal = (propsToPass.target === '_blank')

	const classes = classNames(
		'link',
		className,
		isExternal ? 'link--external' : null)

	if (isExternal && !propsToPass.rel) {
		propsToPass.rel = 'noopener noreferrer'
	}

	return <a className={classes} {...propsToPass}>{children}</a>
}

export default Link