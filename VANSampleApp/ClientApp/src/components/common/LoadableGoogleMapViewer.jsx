﻿import React from 'react'
import Loadable from 'react-loadable'

import Loading from './Loading'

const LoadableGoogleMapViewer = Loadable({
	loading: () => <Loading />,
	loader: () => import('./GoogleMapViewer')
})

export default LoadableGoogleMapViewer