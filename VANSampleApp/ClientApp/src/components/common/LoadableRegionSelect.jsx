﻿import React from 'react'
import Loadable from 'react-loadable'

import Loading from './Loading'

// allows us to code-split RegionDropdown which adds 75 kb to bundle apparently
// TODO: call a REST service to get countries/states instead of relying on 
// RegionDropdown's hard coded data
// https://reactjs.org/docs/code-splitting.html
const LoadableRegionSelect = Loadable({
	loading: () => <Loading />,
	loader: () => import('./RegionSelect')
})

export default LoadableRegionSelect