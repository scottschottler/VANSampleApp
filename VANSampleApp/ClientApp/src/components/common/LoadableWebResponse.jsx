﻿import React from 'react'
import Loadable from 'react-loadable'

import Loading from './Loading'

// allows us to code-split React Json viewer component, which adds a lot to bundle:
// https://reactjs.org/docs/code-splitting.html
const LoadableWebResponse = Loadable({
	loading: () => <Loading />,
	loader: () => import('./WebResponse')
})

export default LoadableWebResponse