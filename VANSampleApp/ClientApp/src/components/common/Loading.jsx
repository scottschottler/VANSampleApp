import React from 'react'
import { componentWillAppendToBody } from 'react-append-to-body'

import './Loading.css'

const Loading = () => (
	<div className="loading-overlay">
		<div className="loading-overlay__spinner loading-spinner"></div>
	</div>
)

export default componentWillAppendToBody(Loading)