﻿import React from 'react'

import Loading from './Loading'
import withServices from './withServices'

export class LoadingContainer extends React.Component {
	constructor(props) {
		super(props)
		props.services.loader.register(this)
		this.state = { isLoading: false }
	}

	show() {
		this.setState({ isLoading: true })
	}

	hide() {
		this.setState({ isLoading: false })
	}

	render() {
		return this.state.isLoading && <Loading />
	}
}

export default withServices(LoadingContainer)