﻿import React from 'react'
import { NavLink as RouterNavLink } from 'react-router-dom'
import classNames from 'classnames'

import './Link.css'

const NavLink = ({ className, ...propsToPass }) => {
	const classes = classNames('nav-link link', className)

	return <RouterNavLink className={classes} {...propsToPass} />		
}

export default NavLink