﻿import React from 'react'
import PropTypes from 'prop-types'
import MaskedInput from 'react-text-mask'

const PhoneInput = ({value, onChange}) => (
	<MaskedInput
		id="phoneNumber"
		name="phoneNumber"
		value={value}
		onChange={onChange}
		className="form-control"
		placeholder="Phone number"
		mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]} />
)

PhoneInput.propTypes = {
	value: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired
}

export default PhoneInput