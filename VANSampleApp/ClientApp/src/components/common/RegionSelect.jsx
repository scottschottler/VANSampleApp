﻿import React from 'react'
import PropTypes from 'prop-types'
import { RegionDropdown } from 'react-country-region-selector'

const RegionSelect = ({ state, onChange }) => (
	<RegionDropdown
		id="stateOrProvince"
		classes="form-control"
		valueType="short"
		defaultOptionLabel="Select State"
		country="United States"
		value={state}
		onChange={onChange} />
)

RegionSelect.propTypes = {
	state: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired
}

export default RegionSelect