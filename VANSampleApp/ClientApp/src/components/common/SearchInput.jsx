﻿import React from 'react'
import { Tooltip, OverlayTrigger } from 'react-bootstrap'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import './SearchInput.css'

class SearchInput extends React.Component {
	handleChange = (event) => {
		const value = event.target.value

		if (this.props.onChange)
			this.props.onChange(value)
	}

	onSearch = (event) => {
		event.preventDefault()

		const search = this.refs.search
		const value = search.value

		this.props.onSearch(value)
	}

	handleClear = () => {
		const search = this.refs.search
		search.focus()

		if (this.props.onChange)
			this.props.onChange('')

		this.props.onSearch('')
	}

	render() {	
		const { className, value, placeholder, tooltip, tooltipPlacement } = this.props

		const clearClasses = classNames(
			'form-search__clear glyphicon glyphicon-remove form-control-feedback',
			{ hidden: this.props.value === '' })

		const searchInput = (
			<form
				className={classNames('form-search', className)}
				onSubmit={this.onSearch}>
				<input
					type="search"
					ref="search"
					value={value}
					onChange={this.handleChange}
					className="form-control form-search__input"
					placeholder={placeholder} />
				<span className={clearClasses} onClick={this.handleClear}></span>
			</form>
		)

		if (tooltip) {
			const toolTip = <Tooltip id="searchTip">{tooltip}</Tooltip>

			return (
				<OverlayTrigger
					placement={tooltipPlacement}
					overlay={toolTip}>
					{searchInput}
				</OverlayTrigger>
			)
		}

		return searchInput
	}
}

SearchInput.propTypes = {
	onSearch: PropTypes.func.isRequired,
	onChange: PropTypes.func.isRequired,
	value: PropTypes.string.isRequired,
	className: PropTypes.string,
	placeholder: PropTypes.string,
	tooltipPlacement: PropTypes.string,
	tooltip: PropTypes.string
}

SearchInput.defaultProps = {
	value: '',
	placeholder: 'Search...',
	tooltipPlacement: 'top'
}

export default SearchInput