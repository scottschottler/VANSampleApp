﻿import React from 'react'

import SearchInput from './SearchInput'

const SearchPeople = props => (
	<SearchInput
		{...props}
		tooltip="Search people by name, email, VAN ID, phone #, address, employer, or occupation"
		placeholder="Search people..." />
)

SearchPeople.propTypes = SearchInput.propTypes

export default SearchPeople