﻿import React from 'react'
import { Button, Tooltip, OverlayTrigger } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { CopyToClipboard } from 'react-copy-to-clipboard'

const TextCopy = ({ text, tooltip }) => (
	<span className="text-copy">
		{text}
		{' '}
		<CopyToClipboard text={text}>
			<OverlayTrigger
				placement="top"
				overlay={<Tooltip id="copyText">{tooltip}</Tooltip>}>
				<Button bsSize="sm">Copy</Button>
			</OverlayTrigger>
		</CopyToClipboard>
	</span>
)

TextCopy.propTypes = {
	text: PropTypes.string.isRequired,
	tooltip: PropTypes.string
}

TextCopy.defaultProps = {
	tooltip: 'Copy to clipboard'
}

export default TextCopy