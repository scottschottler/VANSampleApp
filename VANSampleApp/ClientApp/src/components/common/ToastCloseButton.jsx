﻿import React from 'react'
import { Glyphicon } from 'react-bootstrap'
import PropTypes from 'prop-types'

import Button from './Button'
import './ToastCloseButton.css'

// default close button looked bad in Edge: 
const ToastCloseButton = ({ closeToast }) => (
	<Button
		bsSize="xsmall"
		bsStyle="link"
		className="toast-close-button"
		aria-label="close">
		<Glyphicon glyph="remove" onClick={closeToast} />
	</Button>
)

ToastCloseButton.propTypes = {
	closeToast: PropTypes.func
}

export default ToastCloseButton