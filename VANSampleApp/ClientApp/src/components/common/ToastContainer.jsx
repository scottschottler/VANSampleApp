﻿import React from 'react'
import { ToastContainer as ToastifyContainer } from 'react-toastify'

import ToastCloseButton from './ToastCloseButton'

const ToastContainer = () => (
	<ToastifyContainer
		position="top-right"
		toastClassName="toast"
		closeButton={<ToastCloseButton />} />
)

export default ToastContainer