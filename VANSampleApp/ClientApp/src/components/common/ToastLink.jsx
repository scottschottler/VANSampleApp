﻿import React from 'react'

import './ToastLink.css'
import NavLink from './NavLink'

const ToastLink = ({ to, children }) => (
	<NavLink className="toast-messages__link" to={to}>{children}</NavLink>	
)

ToastLink.propTypes = NavLink.propTypes

export default ToastLink