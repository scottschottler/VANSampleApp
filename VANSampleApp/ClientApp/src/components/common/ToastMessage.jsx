﻿import React from 'react'
import { Glyphicon } from 'react-bootstrap'
import PropTypes from 'prop-types'
import 'react-toastify/dist/ReactToastify.css'

import './ToastMessage.css'

const ToastMessage = ({ type, messages }) => {
	// ensure node PropType is an array for map function:
	const messagesArray = Array.isArray(messages) ? messages : [messages]
	
	return (
		<ol className="toast__messages">
			{messagesArray.map((message, index) => {
				return (
					<li className="toast__message media" key={index}>
						<Glyphicon
							className="toast__icon media-left media-top"
							glyph={type === 'error' ? 'exclamation-sign' : 'ok-sign'} />
						<span className="media-body">{message}</span>
					</li>
				)
			})}
		</ol>
	)
}

ToastMessage.propTypes = {
	messages: PropTypes.node.isRequired,
	type: PropTypes.string,
}

export default ToastMessage