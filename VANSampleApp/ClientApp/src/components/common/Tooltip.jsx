﻿import React from 'react'
import { Tooltip as BootstrapTooltip } from 'react-bootstrap'

import './Tooltip.css'

const Tooltip = (props) => (
	<BootstrapTooltip {...props} />	
)

Tooltip.propTypes = BootstrapTooltip.propTypes

export default Tooltip