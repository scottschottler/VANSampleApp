﻿import React from 'react'
import { Alert } from 'react-bootstrap'
import PropTypes from 'prop-types'
import ReactJson from 'react-json-view'

import IFrame from './IFrame'

const WebResponse = ({ response }) => {
	const { status, data } = response
	const alertStyle = status >= 400 ? 'danger' : 'success'
	const isJSON = typeof data === 'object'

	return (
		<div>
			<Alert bsStyle={alertStyle}>
				<strong>status: {status}</strong>
			</Alert>

			{isJSON ?
				<ReactJson
					src={data}
					name={null}
					enableClipboard={false}
					displayDataTypes={false}
					displayObjectSize={false} />
				:
				<IFrame content={data} />
			}
		</div>
	)
}

WebResponse.propTypes = {
	response: PropTypes.object.isRequired
}

export default WebResponse