﻿import React from 'react'
import { wrapDisplayName } from 'recompose'
import hoistNonReactStatic from 'hoist-non-react-statics'

// import services from '../../services'
import services from '../../services/fake'

// allows injecting services in components via props so the app is easier to test:
export default function withServices(WrappedComponent) {
	const WithServices = (props) => (
		<WrappedComponent services={services} {...props} />
	)

	WithServices.displayName = wrapDisplayName(WrappedComponent, 'withServices')

	hoistNonReactStatic(WithServices, WrappedComponent)

	return WithServices
}