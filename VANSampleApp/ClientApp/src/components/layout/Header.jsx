﻿import React from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import { pure } from 'recompose'

import MenuToggle from './MenuToggle'
import ProfileMenu from './ProfileMenu'
import './Header.css'
import Logo from './logo.svg'

const Header = ({ onToggleMenu }) => (
	<header className="header">
		<MenuToggle onToggleMenu={onToggleMenu} />

		<NavLink className="header__home-link" to="/">
			<img src={Logo} className="header__logo" alt="VAN Sample App" />
			<h1 className="header__app-name">Sample App</h1>
		</NavLink>

		<ProfileMenu className="header__profile-menu" />
	</header>	
)

Header.propTypes = {
	onToggleMenu: PropTypes.func.isRequired
}

export default pure(Header)