import React from 'react'
import { withRouter } from 'react-router'
import classNames from 'classnames'

import Header from './Header'
import Menu from './Menu'
import LoadingContainer from '../common/LoadingContainer'
import ToastContainer from '../common/ToastContainer'
import ErrorBoundary from '../common/ErrorBoundary'
import './Layout.css'

export class Layout extends React.PureComponent {
	state = { isMenuOpen: false }

	closeMenu = () => {
		this.setState({ isMenuOpen: false })
	}

	handleToggleMenu = () => {
		this.setState(prevState => ({ isMenuOpen: !prevState.isMenuOpen }))
	}

	componentDidUpdate(prevProps) {
		if (this.props.location !== prevProps.location) {
			window.scrollTo(0, 0)
			this.closeMenu()
		}
	}

	handleClickOutsideMenu = (e) => {
		if (e.target.id !== 'toggle-menu')
			this.closeMenu()
	}

	render() {
		const currentView = this.props.children
		const { isMenuOpen } = this.state
		const menuClasses = classNames('layout__menu', isMenuOpen && 'layout__menu--open')

		return (
			<div className="layout">
				<Header onToggleMenu={this.handleToggleMenu} />

				<div className="layout__body">
					<Menu
						className={menuClasses}
						disableOnClickOutside={!isMenuOpen}
						onClickOutside={this.handleClickOutsideMenu} />

					<main className="layout__content" role="main">
						<ErrorBoundary>
							{currentView}
						</ErrorBoundary>
					</main>
				</div>

				<LoadingContainer />
				<ToastContainer />
			</div>
		)
	}
}

export default withRouter(Layout)