﻿import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import onClickOutside from 'react-onclickoutside'

import NavMenu from './NavMenu'
import SearchPeople from '../common/SearchPeople'
import withServices from '../common/withServices'
import './Menu.css'

export class Menu extends React.Component {
	constructor(props) {
		super(props)
		this.search = props.services.search
		this.state = { search: '' }
	}

	handleSearch = (search) => {
		if (search) {
			this.search.searchPeople(search)
			this.setState({ search: '' })
		}
	}

	handleSearchChange = (search) => {
		this.setState({ search })
	}

	handleClickOutside = (e) => {
		this.props.onClickOutside(e)
	}

	render() {
		const { className } = this.props
		const menuClasses = classNames('menu', className)

		return (
			<div className={menuClasses}>
				<div className="menu__search">
					<SearchPeople
						onSearch={this.handleSearch}
						onChange={this.handleSearchChange}
						value={this.state.search}
						tooltipPlacement="right" />
				</div>

				<NavMenu />
			</div>
		)
	}
}

Menu.propTypes = {
	className: PropTypes.string,
	onClickOutside: PropTypes.func
}

export default withServices(onClickOutside(Menu))