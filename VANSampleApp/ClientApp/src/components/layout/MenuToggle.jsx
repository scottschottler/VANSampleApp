﻿import React from 'react'
import { Glyphicon } from 'react-bootstrap'
import PropTypes from 'prop-types'

import './MenuToggle.css'

const MenuToggle = ({ onToggleMenu }) => (
	<a className="toggle-menu" onClick={onToggleMenu}>
		<Glyphicon id="toggle-menu" glyph="menu-hamburger" />
	</a>
)

MenuToggle.propTypes = {
	onToggleMenu: PropTypes.func.isRequired
}

export default MenuToggle