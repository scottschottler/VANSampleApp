﻿import React from 'react'
import { pure } from 'recompose'
import { withRouter } from 'react-router'
import { Glyphicon } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import classNames from 'classnames'

import './NavMenu.css'

const NavMenuLink = ({ to, glyph, children }) => (
	<NavLink
		className="nav-menu__link"
		activeClassName="nav-menu__link--active"
		to={to} exact>
		<Glyphicon className="nav-menu__icon" glyph={glyph} />
		<span className="nav-menu__link-text">{children}</span>
	</NavLink>
)

const NavMenu = ({ className }) => (
	<nav className={classNames('nav-menu', className)}>
		<NavMenuLink to="/" glyph="book">Read Me</NavMenuLink>
		<NavMenuLink to="/apitest" glyph="wrench">API Test</NavMenuLink>
		<NavMenuLink to="/people" glyph="user">People</NavMenuLink>
		<NavMenuLink to="/locations" glyph="map-marker">Locations</NavMenuLink>
		<NavMenuLink to="/reports" glyph="stats">Data Visualization</NavMenuLink>
	</nav>	
)

// only re-render when props.location changes:
export default withRouter(pure(NavMenu))