﻿import React from 'react'
import { PropTypes } from 'prop-types'
import { Glyphicon, OverlayTrigger } from 'react-bootstrap'
import classNames from 'classnames'

import Tooltip from '../common/Tooltip'
import './ProfileMenu.css'

const ProfileMenu = ({ className }) => (
	<OverlayTrigger
		placement="bottom"
		overlay={<Tooltip id="logout">TODO: wire up Auth0 JWT</Tooltip>}>
		<div className={classNames('pull-right profile-menu', className)}>
			<Glyphicon glyph="user" />
			<a className="profile-menu__logout">Logout</a>
		</div>
	</OverlayTrigger>
)

ProfileMenu.propTypes = {
	className: PropTypes.string
}

export default ProfileMenu