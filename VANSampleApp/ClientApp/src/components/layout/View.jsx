﻿import React from 'react'
import { Panel } from 'react-bootstrap'
import PropTypes from 'prop-types'
import classNames from 'classnames'

import withServices from '../common/withServices'
import findByType from '../../services/findByType'
import './View.css'

const Breadcrumb = () => null

export class View extends React.Component {
	constructor(props) {
		super(props)
		this.toast = props.services.toast
	}

	componentWillUnmount() {
		this.toast.dismiss()
	}

	render() {
		const { title, children, className } = this.props
		const classes = classNames('view', className)
		const breadcrumb = findByType(children, Breadcrumb)

		return (
			<div className={classes}>
				<h5 className="view__title">{title}</h5>

				{breadcrumb && breadcrumb.props.children}

				<Panel>
					<Panel.Body>
						{children}
					</Panel.Body>
				</Panel>
			</div>
		)
	}
}

View.Breadcrumb = Breadcrumb

View.propTypes = {
	className: PropTypes.string,
	title: PropTypes.string.isRequired
}

export default withServices(View)