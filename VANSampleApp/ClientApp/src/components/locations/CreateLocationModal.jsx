﻿import React from 'react'
import { Modal } from 'react-bootstrap'
import PropTypes from 'prop-types'

import Button from '../common/Button'
import AddressContainer from '../common/AddressContainer'

class CreateLocationModal extends React.Component {
	state = { location: {}, isAddressValid: false }

	handleAddressChanged = (address, isAddressValid) => {
		this.setState({
			location: {
				name: address.name,
				address
			},
			isAddressValid
		})
	}

	handleSaveLocation = () => {
		const location = this.state.location
		this.props.onSave(location)
	}

	render() {
		const { show, onClose } = this.props
		const { isAddressValid } = this.state

		return (
			<Modal show={show} onHide={onClose}>
				<Modal.Header closeButton>
					<Modal.Title>Create New Location</Modal.Title>
				</Modal.Header>

				<Modal.Body>
					<AddressContainer onAddressChanged={this.handleAddressChanged} />
				</Modal.Body>

				<Modal.Footer>
					<div className="text-center">
						<Button onClick={onClose}>Close</Button>
						<Button
							bsStyle="primary"
							onClick={this.handleSaveLocation}
							disabled={!isAddressValid}>
							Save
						</Button>
					</div>
				</Modal.Footer>
			</Modal>
		)
	}
}

CreateLocationModal.propTypes = {
	onClose: PropTypes.func.isRequired,
	onSave: PropTypes.func.isRequired,
	show: PropTypes.bool.isRequired
}

export default CreateLocationModal