﻿import React from 'react'
import PropTypes from 'prop-types'
import { TableHeaderColumn } from 'react-bootstrap-table'

import ViewLocationModal from './ViewLocationModal'
import CreateLocationModal from './CreateLocationModal'
import View from '../layout/View'
import GoogleMapsLink from '../common/GoogleMapsLink'
import DataTable from '../common/DataTable'

class LocationsView extends React.Component {
	renderAddress = (address) => {
		if (address && address.preview)
			return <GoogleMapsLink address={address.preview} />

		return null
	}

	render() {
		const {
			locations,
			options,
			selectedLocation,
			showNewLocation,
			onCloseLocation,
			onSaveLocation
		} = this.props

		const showViewLocation = selectedLocation != null

		return (
			<View title="Locations" className="locations">
				<CreateLocationModal
					show={showNewLocation}
					onClose={onCloseLocation}
					onSave={onSaveLocation} />

				<ViewLocationModal
					show={showViewLocation}
					location={selectedLocation}
					onClose={onCloseLocation} />

				<DataTable
					options={options}
					data={locations}
					deleteRow
					pagination
					csvFileName="Locations.csv">
					<TableHeaderColumn
						isKey
						dataField="locationId"
						csvHeader="Location ID"
						hidden
						export>
						Location ID
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="name"
						csvHeader="Name"
						dataSort>
						Name
					</TableHeaderColumn>
					<TableHeaderColumn
						dataField="address"
						export={false}
						dataFormat={this.renderAddress}>
						Address
					</TableHeaderColumn>

					{/* hidden columns are just for csv export */}
					<TableHeaderColumn
						dataField="addressLine1"
						csvHeader="Address Line 1"
						hidden
						export />
					<TableHeaderColumn
						dataField="city"
						csvHeader="City"
						hidden
						export />
					<TableHeaderColumn
						dataField="stateOrProvince"
						csvHeader="State"
						hidden
						export />
					<TableHeaderColumn
						dataField="zipOrPostalCode"
						csvHeader="Zip"
						hidden
						export />
				</DataTable>
			</View>
		)
	}
}

LocationsView.propTypes = {
	locations: PropTypes.array.isRequired,
	options: PropTypes.object.isRequired,
	onCloseLocation: PropTypes.func.isRequired,
	onSaveLocation: PropTypes.func.isRequired,
	selectedLocation: PropTypes.object,
	showNewLocation: PropTypes.bool
}

LocationsView.defaultProps = {
	showNewLocation: false
}

export default LocationsView