﻿import React from 'react'
import queryString from 'query-string'

import LocationsView from './LocationsView'
import ToastLink from '../common/ToastLink'
import withServices from '../common/withServices'

export class LocationsViewContainer extends React.Component {
	constructor(props) {
		super(props)
		const { locationService, toast, search, geocoder } = this.props.services

		this.locationService = locationService
		this.toast = toast
		this.search = search
		this.geocoder = geocoder

		this.state = {
			search: '',
			locations: [],
			selectedLocation: null,
			totalLocations: 0,
			top: 10,
			skip: 0
		}
	}

	_isMounted = false

	componentDidMount() {
		this._isMounted = true
		this.loadLocations()
	}

	componentWillUnmount() {
		this._isMounted = false
	}

	componentDidUpdate(prevProps) {
		if (prevProps.location.search !== this.props.location.search) {
			this.loadLocations()
		} else if (prevProps.match.params.locationId !== this.props.match.params.locationId) {
			if (this.props.match.params.locationId !== 'new' &&
				prevProps.match.params.locationId !== 'new') {
				this.setSelectedLocation()
			}
		}
	}

	loadLocations = () => {
		const newState = {
			search: '',
			top: 10,
			skip: 0
		}

		if (this.props.location.search) {
			const values = queryString.parse(this.props.location.search)

			if (values.search)
				newState.search = values.search

			if (values.top)
				newState.top = parseInt(values.top, 10)

			if (values.skip)
				newState.skip = parseInt(values.skip, 10)
		}

		this.setState(newState)

		const options = newState

		this.locationService.getLocations(options, response => {
			if (this._isMounted) {
				const json = response.data
				const locations = this.flattenLocations(json.items)

				this.setState({
					locations,
					totalLocations: json.count
				}, this.setSelectedLocation)
			}
		})
	}

	flattenLocations(locations) {
		return locations.map(l => {
			// So csv export doesn't show undefined:
			const defaultAddress = {
				addressLine1: '',
				city: '',
				stateOrProvince: '',
				zipOrPostalCode: ''
			}

			const address = l.address || defaultAddress

			return Object.assign(l, { ...address })
		})
	}

	setSelectedLocation = () => {
		const locations = this.state.locations
		const locationId = this.props.match.params.locationId
		
		if (locationId && locationId !== 'new') {
			let selectedLocation = locations.find(l => l.locationId === +locationId)

			if (selectedLocation) {
				this.setState({ selectedLocation })
			} else {
				// if they navigated directly to it by entering the url manually and the location was not found in current page of results:
				this.locationService.getLocation(
					locationId,
					response => {
						selectedLocation = response.data
						this.setState({ selectedLocation })
					})
			}
		} else {
			this.setState({ selectedLocation: null })
		}
	}

	getQueryString = () => this.props.location.search || ''

	handleRowClick = (location) => {
		this.props.history.push(`/locations/${location.locationId}${this.getQueryString()}`)
	}

	handleCloseLocation = () => {
		const url = '/locations' + this.getQueryString()
		this.props.history.push(url)
		this.setState({ selectedLocation: null })
	}

	handleNewClick = () => {
		this.props.history.push('/locations/new' + this.getQueryString())
	}

	handleSaveLocation = (location) => {
		this.locationService.saveLocation(location, response => {
			const locationId = parseInt(response.data, 10)
			const toastMessage = (
				<span>
					<ToastLink to={`locations/${locationId}`}>Location {locationId}</ToastLink>{' '}
					successfully created!
				</span>
			)
			this.toast.success(toastMessage)

			this.props.history.push('/locations')

			// because component update method checks search, not pathname
			this.loadLocations()
		})
	}

	handleDelete = (locationIds) => {
		this.locationService.deleteLocations(locationIds, response => {
			if (this.state.skip) {
				this.props.history.push('/locations')
			} else {
				const locations = this.state.locations.filter(l => !locationIds.includes(l.locationId))
				this.setState({ locations, totalLocations: locations.length })
			}
		})
	}

	handleSearchChange = (search) => this.setState({ search })

	handleSearch = (search) => {
		this.search.searchLocations(search)
	}

	calculateCurrentPage = () => {
		const { skip, top } = this.state
		const page = skip ? ((skip / top) + 1) : 1
		return page
	}

	handlePageChange = (page, sizePerPage) => {
		const params = { top: sizePerPage }

		if (this.state.search)
			params.name = this.state.search

		const skip = (page - 1) * sizePerPage

		if (skip > 0)
			params.skip = skip

		const qs = queryString.stringify(params)

		this.props.history.push('/locations?' + qs)
	}

	render() {
		const { totalLocations, locations, selectedLocation } = this.state
		const { locationId } = this.props.match.params

		const showNewLocation = (locationId && locationId.toLowerCase() === 'new') || false
		
		const options = {
			isSelectable: true,
			newButtonText: 'New Location',
			onNewClick: this.handleNewClick,
			onSearch: this.handleSearch,
			onSearchChange: this.handleSearchChange,
			searchPlaceholder: 'Enter location name...',
			searchValue: this.state.search,
			onDeleteRow: this.handleDelete,
			onRowClick: this.handleRowClick,
			onPageChange: this.handlePageChange,
			sizePerPage: this.state.top,
			page: this.calculateCurrentPage(),
			totalDataSize: totalLocations
		}

		return (
			<LocationsView
				locations={locations}
				options={options}
				selectedLocation={selectedLocation}
				showNewLocation={showNewLocation}
				onCloseLocation={this.handleCloseLocation}
				onSaveLocation={this.handleSaveLocation} />
		)
	}
}

export default withServices(LocationsViewContainer)