﻿import React from 'react'
import PropTypes from 'prop-types'
import { Modal, Alert } from 'react-bootstrap'

import Button from '../common/Button'
import TextCopy from '../common/TextCopy'
import LoadableGoogleMapViewer from '../common/LoadableGoogleMapViewer'

const ViewLocationModal = ({ show, onClose, location }) => {
	if (location) {
		const hasAddress = location.address && location.address.preview
		const hasGeoCoordinates = location.address && location.address.geoLocation

		return (
			<Modal show={show} onHide={onClose}>
				<Modal.Header closeButton>
					<Modal.Title>{location.name}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{hasAddress
						? <TextCopy text={location.address.preview} tooltip="Copy address to clipboard" />
						: <div>No address details</div>
					}

					<div style={{ marginTop: 10 }}>
						{hasGeoCoordinates
							? <LoadableGoogleMapViewer
								lat={location.address.geoLocation.lat}
								lon={location.address.geoLocation.lon} />
							: <Alert bsStyle="info">Can't display google map without lat/lon coordinates</Alert>
						}
					</div>
				</Modal.Body>
				<Modal.Footer>
					<div className="text-center">
						<Button onClick={onClose}>Close</Button>
					</div>
				</Modal.Footer>
			</Modal>
		)
	}

	return null
}

ViewLocationModal.propTypes = {
	onClose: PropTypes.func.isRequired,
	show: PropTypes.bool.isRequired,
	location: PropTypes.object
}

export default ViewLocationModal