﻿import React from 'react'
import { Alert, ControlLabel, Breadcrumb, FormControl } from 'react-bootstrap'
import PropTypes from 'prop-types'

import View from '../layout/View'
import Button from '../common/Button'
import AddressContainer from '../common/AddressContainer'
import EmailInput from '../common/EmailInput'
import PhoneInput from '../common/PhoneInput'
import FormGroupTitle from '../common/FormGroupTitle'
import NavLink from '../common/NavLink'
import FormGroup from '../common/FormGroup'

const CreatePersonView = ({ onSubmit, onChange, onAddressChanged, error, input, validate }) => {
	const { errors, isValid } = error

	return (
		<View title="New Person" className="people-create">
			<View.Breadcrumb>
				<Breadcrumb>
					<Breadcrumb.Item componentClass="span">
						<NavLink to="/people">People</NavLink>
					</Breadcrumb.Item>
					<Breadcrumb.Item active>New</Breadcrumb.Item>
				</Breadcrumb>
			</View.Breadcrumb>

			<Alert bsStyle="danger">
				This view is not currently working. TODO: troubleshoot VAN API request
			</Alert>

			<form onSubmit={onSubmit}>
				<FormGroup
					controlId="firstName"
					validationState={validate.firstName && errors.firstName && 'error'}>
					<ControlLabel>First Name</ControlLabel>
					<FormControl
						type="text"
						value={input.firstName}
						onChange={onChange}
						placeholder="First Name"
						maxLength="20" />
				</FormGroup>
				<FormGroup
					controlId="lastName"
					validationState={validate.lastName && errors.lastName && 'error'}>
					<ControlLabel>Last Name</ControlLabel>
					<FormControl
						type="text"
						value={input.lastName}
						onChange={onChange}
						placeholder="Last Name"
						maxLength="25" />
				</FormGroup>
				<FormGroup controlId="dateOfBirth">
					<ControlLabel>Birthday</ControlLabel>
					<FormControl
						type="date"
						value={input.dateOfBirth}
						onChange={onChange} />
				</FormGroup>

				<fieldset>
					<FormGroupTitle>Contact</FormGroupTitle>
					<FormGroup controlId="phoneNumber">
						<ControlLabel>Phone Number</ControlLabel>
						<PhoneInput
							value={input.phoneNumber}
							onChange={onChange} />
					</FormGroup>
					<FormGroup controlId="email">
						<ControlLabel>Email</ControlLabel>
						<EmailInput
							value={input.email}
							onChange={onChange} />
					</FormGroup>
				</fieldset>

				<fieldset>
					<FormGroupTitle>Details</FormGroupTitle>
					<FormGroup controlId="party">
						<ControlLabel>Party</ControlLabel>
						<FormControl
							componentClass="select"
							placeholder="select"
							onChange={onChange}
							value={input.party}>
							<option value="D">Democrat</option>
							<option value="R">Republican</option>
						</FormControl>
					</FormGroup>
					<FormGroup controlId="employer">
						<ControlLabel>Employer</ControlLabel>
						<FormControl
							type="text"
							placeholder="Employer"
							onChange={onChange}
							value={input.employer}
							maxLength="50" />
					</FormGroup>
					<FormGroup controlId="occupation">
						<ControlLabel>Occupation</ControlLabel>
						<FormControl
							type="text"
							placeholder="Occupation"
							onChange={onChange}
							value={input.occupation}
							maxLength="50" />
					</FormGroup>
				</fieldset>

				<fieldset>
					<FormGroupTitle>Address</FormGroupTitle>
					<AddressContainer
						validate={false}
						onAddressChanged={onAddressChanged}
						showName={false}
						clearText="Clear Address" />
				</fieldset>

				<FormGroup>
					<NavLink to="/people">
						<Button>Cancel</Button>
					</NavLink>
					{' '}
					<Button disabled={!isValid} bsStyle="primary" type="submit">Save</Button>
				</FormGroup>
			</form>
		</View>
	)
}

CreatePersonView.propTypes = {
	onChange: PropTypes.func.isRequired,
	onSubmit: PropTypes.func.isRequired,
	onAddressChanged: PropTypes.func.isRequired,
	input: PropTypes.object.isRequired,
	error: PropTypes.object.isRequired,
	validate: PropTypes.object
}

CreatePersonView.defaultProps = {
	validate: {}
}

export default CreatePersonView