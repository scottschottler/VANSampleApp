﻿import React from 'react'
import validator from 'validator'

import CreatePersonView from './CreatePersonView'
import withServices from '../common/withServices'

export class CreatePersonViewContainer extends React.Component {
	constructor(props) {
		super(props)

		const { toast, peopleService } = props.services
		this.peopleService = peopleService
		this.toast = toast

		this.state = {
			input: {
				firstName: '',
				lastName: '',
				dateOfBirth: '',
				party: 'D',
				employer: '',
				occupation: '',

				phoneNumber: '',
				email: '',
				address: null
			},
			validate: {},
			isAddressValid: false
		}
	}

	handleChange = (event) => {
		const fieldName = event.target.id || event.target.name
		const fieldValue = event.target.value

		this.setState(prevState => ({
			input: {
				...prevState.input,
				[fieldName]: fieldValue
			},
			validate: {
				...prevState.validate,
				[fieldName]: true
			}
		}))
	}

	handleAddressChanged = (address, isAddressValid) => {
		this.setState(prevState => ({
			input: {
				...prevState.input,
				address
			},
			isAddressValid
		}))
	}

	buildPerson = () => {
		const {
			firstName,
			lastName,
			dateOfBirth,
			party,
			employer,
			occupation,
			phoneNumber,
			email,
			address
		} = this.state.input

		const person = {
			firstName,
			lastName,
			dateOfBirth,
			party,
			employer,
			occupation,
		}
		
		if (validator.isEmail(email)) {
			person.emails = [{ email }]
		}

		if (validator.isMobilePhone(phoneNumber, 'any')) {
			person.phones = [{ phoneNumber }]
		}

		if (this.state.isAddressValid) {
			person.addresses = [address]
		}

		return person
	}

	handleSubmit = (event) => {
		event.preventDefault()

		const person = this.buildPerson()

		this.peopleService.savePerson(person, response => {
			this.props.history.push('/people')

			const message = `${person.firstName} ${person.lastName} saved!`
			this.toast.success(message)
		})
	}

	validate() {
		const errors = {}
		const { firstName, lastName } = this.state.input

		if (validator.isEmpty(firstName))
			errors.firstName = 'First name is required'

		if (validator.isEmpty(lastName))
			errors.lastName = 'Last name is required'

		return {
			errors,
			isValid: Object.keys(errors).length === 0
		}
	}

	render() {
		const error = this.validate()
		const { isAddressValid, ...propsToPass } = this.state

		return (
			<CreatePersonView
				onChange={this.handleChange}
				onSubmit={this.handleSubmit}
				onAddressChanged={this.handleAddressChanged}
				error={error}
				{...propsToPass} />
		)
	}
}

export default withServices(CreatePersonViewContainer)