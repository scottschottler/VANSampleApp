﻿import React from 'react'
import PropTypes from 'prop-types'
import { TableHeaderColumn } from 'react-bootstrap-table'

import UserPopover from './UserPopover'
import View from '../layout/View'
import DataTable from '../common/DataTable'
import Link from '../common/Link'

class PeopleView extends React.Component {
	renderUserPopover = (fullName, person) => <UserPopover person={person} />

	renderPhone = (phone) => {
		if (phone) {
			// stopPropagation so it doesn't click the location row:
			return <Link href={`tel:${phone}`} onClick={(e) => e.stopPropagation()}>{phone}</Link>
		}

		return null
	}

	render() {
		const { people, options } = this.props

		options.newButtonText = 'New Person'
		options.searchPlaceholder = 'Search people...'
		options.searchTooltip = 'Search people by name, email, VAN ID, phone #, address, employer, or occupation'
		
		return (
			<View title="People" className="people">

				<DataTable
					data={people}
					csvFileName="People.csv"
					options={options}>
					<TableHeaderColumn
						isKey
						width="100"
						dataField="vanId"
						csvHeader="VAN ID">
						VAN ID
					</TableHeaderColumn>
					<TableHeaderColumn
						width="120"
						dataField="fullName"
						dataFormat={this.renderUserPopover}
						csvHeader="Name"
						dataSort>
						Name
					</TableHeaderColumn>
					<TableHeaderColumn
						width="120"
						dataField="phone"
						dataFormat={this.renderPhone}
						csvHeader="Phone">
						Phone
					</TableHeaderColumn>
					<TableHeaderColumn
						width="120"
						dataField="addressLine1"
						csvHeader="Address Line 1">
						Address Line 1
					</TableHeaderColumn>
					<TableHeaderColumn
						width="100"
						dataField="city"
						csvHeader="City"
						dataSort>
						City
					</TableHeaderColumn>
					<TableHeaderColumn
						width="75"
						dataField="stateOrProvince"
						csvHeader="State"
						dataSort>
						State
					</TableHeaderColumn>
					<TableHeaderColumn
						width="50"
						dataField="zipOrPostalCode"
						csvHeader="Zip">
						Zip
					</TableHeaderColumn>
				</DataTable>

			</View>
		)
	}
}

PeopleView.propTypes = {
	people: PropTypes.array.isRequired,
	options: PropTypes.object.isRequired
}

export default PeopleView