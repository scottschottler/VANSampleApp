﻿import React from 'react'
import queryString from 'query-string'
import get from 'lodash/get'

import PeopleView from './PeopleView'
import withServices from '../common/withServices'

export class PeopleViewContainer extends React.Component {	
	constructor(props) {
		super(props)
		this.peopleService = props.services.peopleService
		this.search = props.services.search
		this.state = {
			search: '',
			people: []
		}
	}

	_isMounted = false

	componentDidMount() {
		this._isMounted = true
		this.loadPeople()
	}

	componentWillUnmount() {
		this._isMounted = false
	}

	componentDidUpdate(prevProps) {
		if (prevProps.location.search !== this.props.location.search) {
			this.loadPeople()
		}
	}

	loadPeople = () => {
		let search = ''

		if (this.props.location.search) {
			let values = queryString.parse(this.props.location.search)

			if (values.search)
				search = values.search
		}

		this.setState({ search })

		if (search) {
			this.peopleService.searchPeople(search, response => {
				if (this._isMounted) {
					const people = this.flattenPeople(response.data)
					this.setState({ people })
				}
			})
		} else {
			this.peopleService.getPeople(response => {
				if (this._isMounted) {
					const people = this.flattenPeople(response.data)
					this.setState({ people })
				}
			})
		}
	}

	flattenPeople(people) {
		// need to flatten for table
		// https://github.com/AllenFang/react-bootstrap-table/issues/1066
		return people.map(p => {
			p.fullName = `${p.firstName} ${p.lastName}`
			
			p.phone = get(p, 'phones[0].phoneNumber', '')
			p.email = get(p, 'emails[0].email', '')

			// So csv export doesn't show undefined:
			const defaultAddress = {
				addressLine1: '',
				city: '',
				stateOrProvince: '',
				zipOrPostalCode: ''
			}
			
			p.address = get(p, 'addresses[0]', defaultAddress)

			return Object.assign(p, { ...p.address })
		})
	}

	handleNewClick = () => {
		this.props.history.push('/people/new')
	}

	handleSearch = (search) => {
		this.search.searchPeople(search)
	}

	handleSearchChange = (search) => {
		this.setState({ search })
	}

	render() {
		const { people } = this.state

		const options = {
			onNewClick: this.handleNewClick,
			onSearch: this.handleSearch,
			onSearchChange: this.handleSearchChange,
			searchValue: this.state.search
		}

		return <PeopleView people={people} options={options} />
	}
}

export default withServices(PeopleViewContainer)