﻿import React from 'react'
import PropTypes from 'prop-types'

import './UserIcon.css'

class UserIcon extends React.Component {
	colors = ['pink', 'green', 'lime', 'amber', 'indigo', 'purple', 'teal']

	alphabetPosition(letter) {
		const code = letter.toUpperCase().charCodeAt(0)
		const position = (code - 64)
		return position
	}

	getColor(letter) {
		const position = this.alphabetPosition(letter)
		const index = Math.ceil(position / 4)
		return this.colors[index]
	}

	render() {
		const { initials } = this.props
		const color = this.getColor(initials[0])

		return <i className={`user-icon user-icon--${color}`}>{initials.toUpperCase()}</i>
	}
}

UserIcon.propTypes = {
	initials: PropTypes.string.isRequired
}

export default UserIcon