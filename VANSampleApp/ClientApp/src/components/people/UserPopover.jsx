﻿import React from 'react'
import { Popover, OverlayTrigger, Glyphicon } from 'react-bootstrap'
import PropTypes from 'prop-types'

import UserIcon from './UserIcon'
import './UserPopover.css'

const UserDetail = ({ glyph, text }) => (
	<li className="user-info__detail media">
		<Glyphicon className="user-info__detail-icon media-left media-top" glyph={glyph} />
		<span className="media-body">{text}</span>
	</li>
)

class UserPopover extends React.Component {
	formatAddress = (a) => {
		return `${a.addressLine1} ${a.city}, ${a.stateOrProvince} ${a.zipOrPostalCode}`
	}

	render() {
		const { fullName, firstName, lastName, phone, address, email } = this.props.person
		
		const initials = firstName[0] + lastName[0]
		const formattedAddress = this.formatAddress(address)
		const hasAddress = formattedAddress.trim().length > 1

		const popover = (
			<Popover id="popup">
				<div className="user-info">
					<UserIcon initials={initials} />
					<h2 className="user-info__name">{fullName}</h2>

					<ul className="user-info__details">
						{phone && <UserDetail glyph="phone" text={phone} />}
						{email && <UserDetail glyph="envelope" text={email} />}
						{hasAddress && <UserDetail glyph="map-marker" text={formattedAddress} />}
					</ul>
				</div>
			</Popover>
		)

		return (
			<OverlayTrigger trigger={['hover', 'focus']} placement="right" overlay={popover}>
				<span>{fullName}</span>
			</OverlayTrigger>
		)
	}
}

UserPopover.propTypes = {
	person: PropTypes.object.isRequired
}

export default UserPopover