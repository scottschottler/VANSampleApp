﻿import React from 'react'
import ReactMarkdown from 'react-markdown'

import View from '../layout/View'
import Loading from '../common/Loading'
import './ReadMeView.css'

class ReadMeView extends React.Component {
	state = {
		isLoading: true,
		markdown: ''
	}

	componentDidMount() {
		this.loadReadMe()
	}

	async loadReadMe() {
		const readMePath = 'https://raw.githubusercontent.com/sschottler/VANSampleApp/master/README.md'

		const response = await fetch(readMePath)
		const markdown = await response.text()

		this.setState({ markdown, isLoading: false })
	}

	render() {
		const { markdown, isLoading } = this.state

		return (
			<View title="Read Me" className="readme">
				{isLoading && <Loading />}
				<ReactMarkdown source={markdown} />
			</View>
		)
	}
}

export default ReadMeView