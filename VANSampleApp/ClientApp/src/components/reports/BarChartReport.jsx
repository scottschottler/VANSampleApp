﻿import React from 'react'
import {
	BarChart,
	Bar,
	Tooltip,
	XAxis,
	YAxis,
	CartesianGrid,
	Label,
	ResponsiveContainer
} from 'recharts'

const BarChartReport = () => {
	const data = [
		{ name: 'Jan 17', total: 12 },
		{ name: 'Feb 17', total: 13 },
		{ name: 'Mar 17', total: 2 },
		{ name: 'Apr 17', total: 8 },
		{ name: 'May 17', total: 9 },
		{ name: 'Jun 17', total: 11 },
		{ name: 'Jul 17', total: 5 },
		{ name: 'Aug 17', total: 7 },
		{ name: 'Sep 17', total: 5 },
		{ name: 'Oct 17', total: 2 }
	]

	return (
		<React.Fragment>
			<strong>New Contacts per Month</strong>

			<ResponsiveContainer width={'100%'} height={270}>
				<BarChart
					data={data}
					margin={{ top: 30, right: 30, left: 0, bottom: 20 }}>

					<CartesianGrid vertical={false} />

					<XAxis dataKey="name" tickLine={false} axisLine={false}>
						<Label
							value="Date Submitted: Month"
							position="bottom"
							style={{ fontWeight: "bold" }} />
					</XAxis>

					<YAxis axisLine={false} tickLine={false}>
						<Label
							angle={-90}
							position="insideLeft"
							style={{ textAnchor: "middle", fontWeight: "bold" }}
							offset={20}>
							Total New Contacts
						</Label>
					</YAxis>

					<Tooltip cursor={{ fill: '#b3e4ff', opacity: 0.5 }} />

					<Bar barSize={40} dataKey="total" fill="#228ae6" />
				</BarChart>
			</ResponsiveContainer>
		</React.Fragment>		
	)
}

export default BarChartReport