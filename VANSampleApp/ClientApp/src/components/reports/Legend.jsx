﻿import React from 'react'
import PropTypes from 'prop-types'

import './Legend.css'

// Recharts legend was breaking PieChart layout on mobile, so I wrote my own:
const Legend = ({ data }) => (
	<ul className="legend">
		{data.map((entry, index) => (
			<li key={index} className="legend__item">
				<span
					className="legend__color"
					style={{ background: entry.color }}></span>
				<span className="legend__text">{entry.name}</span>
			</li>
		))}
	</ul>
)

Legend.propTypes = {
	data: PropTypes.array.isRequired
}

export default Legend