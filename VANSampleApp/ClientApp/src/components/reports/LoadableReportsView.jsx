﻿import React from 'react'
import Loadable from 'react-loadable'

import Loading from '../common/Loading'

// allows us to code-split Recharts, which adds a lot to bundle:
// https://reactjs.org/docs/code-splitting.html
const LoadableReportsView = Loadable({
	loading: () => <Loading />,
	loader: () => import('./ReportsView')
})

export default LoadableReportsView