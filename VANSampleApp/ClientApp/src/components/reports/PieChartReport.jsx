﻿import React from 'react'
import { Row, Col } from 'react-bootstrap'
import { PieChart, Pie, Cell, Tooltip } from 'recharts'

import Legend from './Legend'

const PieChartReport = () => {
	let data = [
		{ name: 'AARP_Protect Medicare Benefits', value: 5, color: '#278de6' },
		{ name: 'Equal Pay Advocacy', value: 4, color: '#354dc4' },
		{ name: 'Stop State Senate Bill 1130', value: 4, color: '#ffc31f' },
		{ name: 'Legalize Sparklers', value: 3, color: '#2e9943' },
		{ name: 'Separation Advocacy Form', value: 3, color: '#8adb99' },
		{ name: 'Social Advocacy', value: 3, color: '#ff5454' },
		{ name: 'Advocacy dp', value: 2, color: '#cfefff' },
		{ name: 'FWI', value: 2, color: '#ff932e' },
		{ name: 'MI Affordable Care Advocacy', value: 2, color: '#18aaba' },
		{ name: 'KING Gen Advo: 5 year offshore drilling', value: 1, color: '#82cc21' }
	].reverse()

	const sum = (prev, next) => prev + next
	const total = data.map(d => d.value).reduce(sum)

	data = data.map(d => {
		d.percent = Math.round((d.value / total) * 100)
		return d
	})

	return (
		<React.Fragment>
			<strong>Top 10 Forms for New Contacts</strong>

			<Row>
				<Col md={6}>
					<div>
						<PieChart
							width={250}
							height={250}
							style={{ margin: 'auto' }}>
							<Pie animationBegin={0}
								data={data}
								innerRadius={'40%'}
								outerRadius={'80%'}>
								{
									data.map((entry, index) => <Cell key={index} fill={entry.color} />)
								}
							</Pie>

							<Tooltip formatter={(value, name, props) => `${value} (${props.payload.percent}%)`} />
						</PieChart>
					</div>
				</Col>
				<Col md={6}>
					<Legend data={data} />
				</Col>
			</Row>

			<strong>Total New Contacts: </strong> <span>{total}</span>
		</React.Fragment>
	)
}

export default PieChartReport