﻿import React from 'react'
import PropTypes from 'prop-types'

import './Report.css'

const Report = ({ title, children }) => (
	<section className="report">
		<div className="report__body">
			{children}
		</div>
		<footer className="report__footer">
			{title}
		</footer>
	</section>
)

Report.propTypes = {
	title: PropTypes.string
}

Report.defaultProps = {
	title: 'Online Activity Report'
}

export default Report