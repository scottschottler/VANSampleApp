﻿import React from 'react'

import Report from './Report'
import BarChartReport from './BarChartReport'
import PieChartReport from './PieChartReport'
import View from '../layout/View'

const ReportsView = () => (
	<View title="Data Visualization" className="reports">
		<React.Fragment>
			<Report>
				<PieChartReport />
			</Report>

			<Report>
				<BarChartReport />
			</Report>
		</React.Fragment>
	</View>
)

export default ReportsView