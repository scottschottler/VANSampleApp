import React from 'react'
import { render } from 'react-dom'
import { Router } from 'react-router-dom'

import App from './App'
import history from './services/history'
import registerServiceWorker from './registerServiceWorker'

const rootElement = document.getElementById('root')

//if (process.env.NODE_ENV !== 'production') {
//	const { whyDidYouUpdate } = require('why-did-you-update')
//	whyDidYouUpdate(React)
//}

render(
	<Router history={history}>
		<App />
	</Router>,
	rootElement)

registerServiceWorker()