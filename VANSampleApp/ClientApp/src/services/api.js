﻿import axios from 'axios'

export default class Api {
	constructor(
		loader,
		errorHandler
	) {
		this.loader = loader
		this.errorHandler = errorHandler

		axios.interceptors.request.use(this.startRequest)
		axios.interceptors.response.use(this.handleSuccess)
	}

	startRequest = (config) => {
		this.loader.show()

		const api = 'api/'
		if (!config.url.startsWith(api))
			config.url = api + config.url

		return config
	}

	handleSuccess = (response) => {
		this.loader.hide()
		return response
	}

	get(url, callback, handleError = true) {
		this.request('get', { url }, callback, handleError)
	}

	post(url, data, callback, handleError = true) {
		this.request('post', { url, data }, callback, handleError)
	}

	delete(url, data, callback, handleError = true) {
		this.request('delete', { url, data }, callback, handleError)
	}

	put(url, data, callback, handleError = true) {
		this.request('put', { url, data }, callback, handleError)
	}

	request(method, config, callback, handleError = true) {
		axios({ method, ...config })
			.then(response => callback && callback(response))
			.catch(e => {
				this.loader.hide()

				if (handleError && e.response && e.response.status >= 400) {
					this.errorHandler(e)
				} else if (callback) {
					callback(e.response)
				}
			})
	}
}