﻿import history from './history'
import Toast from './toast'

export default (error) => {
	if (error.response && error.response.status === 404) {
		// redirect to home:
		history.push('/')
	} else {
		let messages = []
		let autoClose = false

		if (error.response && error.response.status === 400) {
			const json = error.response.data
			console.log(json)

			// ModelStateInvalidFilter returns 400 and json like:
			// { firstName: [ "error1", "error2"], lastName: [ "error1" ] }

			// VANExceptionFilter returns 400 and json like:
			// { INVALID_PARAMETER: ["text description for end users"] }

			messages = Object.values(json).reduce((a, b) => a.concat(b))
		} else {
			console.error(error)
			autoClose = true
			messages = 'An error occurred.'
		}
		Toast.error(messages, autoClose)
	}
}