﻿export const people = [
	{
		vanId: '33333333',
		firstName: 'Scott',
		lastName: 'Schottler',
		phones: [
			{ phoneNumber: '555-555-5555' }
		],
		emails: [
			{ email: 'scottschottler@gmail.com' }
		],
		addresses: [
			{
				addressLine1: '2301 Columbia Pike',
				city: 'Arlington',
				stateOrProvince: 'VA',
				zipOrPostalCode: '22204'
			}
		]
	},
	{
		vanId: '444444444',
		firstName: 'Barack',
		lastName: 'Obama',
		phones: [
			{ phoneNumber: '555-555-5555' }
		],
		emails: [
			{ email: 'barackobama@gmail.com' }
		],
		addresses: [
			{
				addressLine1: '1234 somewhere street',
				city: 'Chicago',
				stateOrProvince: 'IL',
				zipOrPostalCode: '22204'
			}
		]
	},
	{
		vanId: '55555555',
		firstName: 'Somebody',
		lastName: 'Else'
	}
]

export const locations = {
	items: [
		{
			locationId: 1,
			name: 'Location 1',
			address: {
				preview: '2301 Columbia Pike, Arlington, VA 22204',
				addressLine1: '2301 Columbia Pike',
				city: 'Arlington',
				stateOrProvince: 'VA',
				zipOrPostalCode: '22204',
				countryCode: 'US',
				geoLocation: {
					lat: 38.862,
					lon: -77.087
				}
			}
		},
		{
			locationId: 2,
			name: 'Location 2',
			address: {
				preview: '443 New York Ave NW, Washington, DC 20001',
				addressLine1: '443 New York Ave NW',
				city: 'Washington',
				stateOrProvince: 'DC',
				zipOrPostalCode: '20001',
				countryCode: 'US'
			}
		}
	],
	count: 2
}