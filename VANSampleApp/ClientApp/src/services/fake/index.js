﻿import { Services } from '../index'
import { people, locations } from './data'

class Api {
	post(url, data, callback, handleError = true) {
		//callback({ status: 200, data: '<span>some html <strong>bla blabla</strong></span>'})
		callback({ status: 200, data: { from: 'fake api service', time: Date.now() } })
	}
}

class PeopleService {
	constructor(loader) {
		this.loader = loader
	}

	getPeople(callback) {
		this.loader.show()
		setTimeout(() => {
			this.loader.hide()
			callback({ data: people })
		}, 500)
	}

	searchPeople(search, callback) {
		search = search.toLowerCase().trim()

		const results = people.filter(p => {
			const fullName = `${p.firstName} ${p.lastName}`.toLowerCase()
			return fullName.includes(search) || p.vanId.includes(search)
		})

		callback({ data: results })
	}

	savePerson(person, callback) {
		console.log(person)

		person.vanId = '5555555'
		people.push(person)

		callback({ vanId: person.vanId, HttpCode: 201, Status: 'Stored' })
	}
}

class LocationService {
	constructor(loader) {
		this.loader = loader
		this.locations = locations
	}

	getLocation(locationId, callback) {
		const location = this.locations.items.filter(l => l.locationId === locationId)[0]

		if (!location) 
			throw new Error('Location not found')

		callback({ data: location })
	}

	getLocations({ search, skip, top }, callback) {
		this.loader.show()

		let results = { ...this.locations }

		if (search) {
			search = search.toLowerCase().trim()
			results.items = results.items.filter(l => l.name.toLowerCase().includes(search))
		}

		const start = skip || 0
		const end = top + start

		results.items = results.items.slice(start, end)
		results.count = this.locations.items.length

		setTimeout(() => {
			this.loader.hide()
			callback({ data: results })
		}, 500)
	}

	saveLocation(location, callback) {
		location.locationId = 33333
		const { addressLine1, city, stateOrProvince, zipOrPostalCode } = location.address
		location.address.preview = `${addressLine1}, ${city}, ${stateOrProvince} ${zipOrPostalCode}`
		this.locations.items.push(location)
		this.locations.count++

		console.log(location)

		callback({ data: location.locationId })
	}

	deleteLocations(locationIds, callback) {
		this.locations.items = this.locations.items.filter(l => !locationIds.includes(l.locationId))
		this.locations.count = this.locations.items.count
		callback({ status: 204 })
	}
}

class FakeServices extends Services  {
	constructor() {
		super()

		console.log('using fake services')

		// override anything in base Services we want to fake:
		this.api = new Api()
		this.peopleService = new PeopleService(this.loader)
		this.locationService = new LocationService(this.loader)
	}
}

export default new FakeServices()