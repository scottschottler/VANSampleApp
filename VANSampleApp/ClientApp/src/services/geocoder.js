﻿export default class geocoder {
	static geocode(address) {
		const url = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(address)}`

		return new Promise((resolve, reject) => {
			fetch(url).then(response => {
				return response.json()
			}).then(data => {
				if (data.status.toUpperCase() === 'OK') {
					const { lat, lng } = data.results[0].geometry.location
					resolve({ geo: { lat, lon: lng } })
				} else {
					reject(false)
				}
			}).catch(e => {
				reject(e)
			})
		})
	}
}