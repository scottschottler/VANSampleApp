﻿import Api from './api'
import loader from './loader'
import errorHandler from './errorHandler'
import PeopleService from './peopleService'
import LocationService from './locationService'
import geocoder from './geocoder'
import search from './search'
import toast from './toast'
import history from './history'

export class Services {
	constructor() {
		this.loader = loader
		this.toast = toast
		this.search = search
		this.history = history
		this.api = new Api(this.loader, errorHandler)
		this.peopleService = new PeopleService(this.api)
		this.locationService = new LocationService(this.api)
		this.geocoder = geocoder
	}
}

export default new Services()