﻿class Loader {
	loadingOverlay = null

	register(loadingOverlay) {
		this.loadingOverlay = loadingOverlay
	}

	show() {
		this.loadingOverlay.show()
	}

	hide() {
		this.loadingOverlay.hide()
	}
}

export default new Loader()