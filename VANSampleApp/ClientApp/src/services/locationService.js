﻿import queryString from 'query-string'

export default class LocationService {
	constructor(api) {
		this.api = api
	}

	getLocation(locationId, callback) {
		this.api.get(`locations/${locationId}`, callback)
	}

	getLocations({ search, skip, top }, callback) {
		let parameters = {}

		if (search)
			parameters.search = search

		if (skip)
			parameters.skip = skip

		if (top)
			parameters.top = top

		let url = 'locations'

		if (Object.keys(parameters).length !== 0) {
			const qs = queryString.stringify(parameters)
			url += `?${qs}`
		}

		this.api.get(url, callback)
	}

	saveLocation(location, callback) {
		this.api.post('locations', location, callback)
	}

	deleteLocations(locationIds, callback) {
		this.api.delete('locations', locationIds, callback)
	}
}