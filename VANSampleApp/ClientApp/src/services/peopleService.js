﻿export default class PeopleService {
	constructor(api) {
		this.api = api
	}

	getPeople(callback) {
		this.api.get('people', callback)
	}

	searchPeople(search, callback) {
		this.api.get(`people?search=${search}`, callback)
	}

	savePerson(person, callback) {
		this.api.post('people', person, callback)
	}
}