﻿import history from './history'

export default class Search {
	static searchPeople(query) {
		Search.search('/people', query)
	}

	static searchLocations(query) {
		Search.search('/locations', query)
	}

	static search(url, query) {
		if (query)
			url += `?search=${encodeURIComponent(query)}`
		history.push(url)
	}
}