﻿import React from 'react'
import { toast } from 'react-toastify'

import ToastMessage from '../components/common/ToastMessage'

export default class Toast {
	static success(messages, autoClose = 3000) {
		toast.success(<ToastMessage messages={messages} />, { autoClose })
	}

	static error(messages, autoClose = 3000) {	
		toast.error(<ToastMessage type="error" messages={messages} />, { autoClose })
	}

	static dismiss() {
		toast.dismiss()
	}
}