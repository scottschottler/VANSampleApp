﻿namespace VANSampleApp.Config
{
	public class AzureSearchSettings
	{
		public string PeopleIndexName { get; set; }
		public string ServiceName { get; set; }
		public string AdminKey { get; set; }
		public string QueryKey { get; set; }

		public bool Configured
		{
			get
			{
				if (string.IsNullOrEmpty(PeopleIndexName) ||
					string.IsNullOrEmpty(ServiceName) ||
					string.IsNullOrEmpty(AdminKey) ||
					string.IsNullOrEmpty(QueryKey))
					return false;

				return true;
			}
		}
	}
}