﻿namespace VANSampleApp.Config
{
	public class RedisCacheSettings
	{
		public string ConnectionString { get; set; }
		public bool Configured => !string.IsNullOrEmpty(ConnectionString);
	}
}