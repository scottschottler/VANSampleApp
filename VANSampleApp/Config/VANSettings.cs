﻿namespace VANSampleApp.Config
{
	public class VANSettings
	{
		public string ServiceURL { get; set; }
		public string ApplicationName { get; set; }
		public string APIKey { get; set; }

		public bool Configured
		{
			get
			{
				if (string.IsNullOrEmpty(ServiceURL) ||
					string.IsNullOrEmpty(ApplicationName) ||
					string.IsNullOrEmpty(APIKey))
					return false;

				return true;
			}
		}
	}
}