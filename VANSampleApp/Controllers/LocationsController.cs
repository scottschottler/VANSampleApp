﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VANSampleApp.Models;
using VANSampleApp.Services;

namespace VANSampleApp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class LocationsController : ControllerBase
	{
		private readonly ILocationService _locationService;

		public LocationsController(ILocationService locationService) => _locationService = locationService;
		
		// GET: api/Locations
		[HttpGet]
		public async Task<PagedResponse<Location>> GetAsync(string search, int? top, int? skip)
		{
			var locations = await _locationService.GetLocationsAsync(search, top, skip);
			return locations;
		}

		// GET: api/Locations/5
		[HttpGet("{locationId}")]
		public async Task<IActionResult> GetAsync(int locationId)
		{
			var location = await _locationService.GetLocationAsync(locationId);

			if (location == null)
				return NotFound();
	
			return Ok(location);
		}

		// POST: api/Locations
		[HttpPost]
		public async Task<IActionResult> PostAsync([FromBody] Location location)
		{
			var locationId = await _locationService.SaveLocationAsync(location);
			return CreatedAtAction(nameof(GetAsync), new { locationId }, locationId);
		}

		// DELETE: api/Locations
		[HttpDelete]
		public async Task<IActionResult> DeleteAsync([FromBody] IEnumerable<int> locationIds)
		{
			var tasks = locationIds.Select(id => _locationService.DeleteAsync(id));
			var results = await Task.WhenAll(tasks);

			if (results.Contains(false))
				return StatusCode((int)HttpStatusCode.InternalServerError);

			return NoContent();
		}
	}
}