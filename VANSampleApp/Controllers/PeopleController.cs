﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VANSampleApp.Models;
using VANSampleApp.Services;

namespace VANSampleApp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class PeopleController : ControllerBase
	{
		private readonly IPeopleService _peopleService;
		private readonly ISearchService _searchService;
		private readonly ICacheService _cacheService;
		private readonly IHostingEnvironment _hostingEnvironment;

		public PeopleController(
			IPeopleService peopleService, 
			ISearchService searchService, 
			ICacheService cacheService,
			IHostingEnvironment hostingEnvironment)
		{
			_peopleService = peopleService;
			_searchService = searchService;
			_cacheService = cacheService;
			_hostingEnvironment = hostingEnvironment;
		}
		
		// GET: api/People
		[HttpGet]
		public IEnumerable<Person> Get(string search)
		{
			return string.IsNullOrEmpty(search) ? _peopleService.GetPeople() : _peopleService.Search(search);
		}
		
		// POST: api/People
		[HttpPost]
		public async Task<IActionResult> PostAsync([FromBody] Person person)
		{
			var match = await _peopleService.SavePersonAsync(person);
			return StatusCode((int)match.HttpCode, match);
		}

		// only used for dev testing
		// GET: api/People/Clear
		// GET: api/People/Clear?rebuild=true 
		[HttpGet("Clear")]
		public async Task<IActionResult> Clear(bool rebuild = false)
		{
			if (_hostingEnvironment.IsDevelopment())
			{
				await _cacheService.SetPeopleAsync(null);

				_searchService.DeletePeopleIndex();

				if (rebuild)
				{
					var people = new FakePeopleService().GetPeople();

					var indexTasks = people.Select(p => _searchService.IndexPersonAsync(p));

					await Task.WhenAll(indexTasks);
				}
			}

			return NoContent();
		}
	}
}