﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using VANSampleApp.Models;
using VANSampleApp.Services;

namespace VANSampleApp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class VANController : ControllerBase
	{
		private readonly IVANClientService _vanClient;

		public VANController(IVANClientService vanClient) => _vanClient = vanClient;
	
		// POST: api/VAN
		[HttpPost]
		public async Task<IActionResult> PostAsync([FromBody] VANRequest request)
		{
			var response = await _vanClient.RequestAsync(request);
			var content = await response.Content.ReadAsStringAsync();
			return StatusCode((int)response.StatusCode, content);
		}
	}
}