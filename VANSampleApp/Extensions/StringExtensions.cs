﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace VANSampleApp.Extensions
{
	public static class StringExtensions
	{
		// replace any special characters or multiple whitespace with a single space
		public static string ToAlphaNumeric(this string value) 
			=> Regex.Replace(value, @"[^0-9a-zA-Z]+", " ");

		// split a string into phrases, for searching
		public static IEnumerable<string> ToTerms(this string value)
			=> ToAlphaNumeric(value).ToLower().Split(' ').Where(s => !string.IsNullOrEmpty(s));
	}
}