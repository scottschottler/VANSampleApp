﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using VANSampleApp.Models;

namespace VANSampleApp.Filters
{
	public class VANExceptionFilter : IExceptionFilter
	{
		private readonly ILogger<VANExceptionFilter> _logger;

		public VANExceptionFilter(ILogger<VANExceptionFilter> logger) => _logger = logger;
		
		public void OnException(ExceptionContext context)
		{
			var e = context.Exception;

			if (e is VANException)
			{
				_logger.LogError(e, e.Message);
				var errors = ((VANException)e).Errors;

				foreach (var error in errors)
					context.ModelState.AddModelError(error.Code, error.Text);

				// match behavior of [ApiControllerAttribute] modelState filter:
				// https://www.strathweb.com/2018/02/exploring-the-apicontrollerattribute-and-its-features-for-asp-net-core-mvc-2-1/
				context.Result = new BadRequestObjectResult(context.ModelState);
				context.ExceptionHandled = true;
			}
		}
	}
}