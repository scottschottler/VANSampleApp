﻿using Newtonsoft.Json;

namespace VANSampleApp.Models
{
	public class Address
	{
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public int? AddressId { get; set; }

		public string AddressLine1 { get; set; }

		public string AddressLine2 { get; set; }

		public string City { get; set; }

		public string StateOrProvince { get; set; }

		public string ZipOrPostalCode { get; set; }

		public string CountryCode { get; set; }

		public GeoLocation GeoLocation { get; set; }

		public string Preview { get; set; }
	}
}