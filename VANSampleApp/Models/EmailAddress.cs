﻿using System.ComponentModel.DataAnnotations;

namespace VANSampleApp.Models
{
	public class EmailAddress
	{
		public EmailAddress(string email) => Email = email;

		[Required]
		public string Email { get; set; }

		//public string Type { get; set; }

		//public bool IsPreferred { get; set; }
	}
}