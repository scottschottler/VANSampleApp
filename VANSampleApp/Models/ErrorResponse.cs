﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VANSampleApp.Models
{
	public class VANException : Exception
	{
		public IEnumerable<Error> Errors { get; private set; }
		public VANException(IEnumerable<Error> errors)
			: base(string.Join("\n", errors.Select(e => e.Text))) => Errors = errors;
	}

	public class ErrorResponse
	{
		public IEnumerable<Error> Errors { get; set; }
	}

	public class Error
	{
		public string Code { get; set; }
		public string Text { get; set; }
		public IEnumerable<string> Properties { get; set; }
	}
}