﻿namespace VANSampleApp.Models
{
	public class GeoLocation
	{
		public decimal Lon { get; set; }
		public decimal Lat { get; set; }

		public GeoLocation(decimal latitude, decimal longitude)
		{
			Lat = latitude;
			Lon = longitude;
		}
	}
}