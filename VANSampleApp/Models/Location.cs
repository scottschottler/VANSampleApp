﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace VANSampleApp.Models
{
	public class Location
	{
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public int? LocationId { get; set; }

		[MaxLength(50)]
		public string Name { get; set; }

		public string DisplayName { get; set; }

		public Address Address { get; set; }
	}
}