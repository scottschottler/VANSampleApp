﻿using System.Collections.Generic;

namespace VANSampleApp.Models
{
	public class PagedResponse<Child>
	{
		public IEnumerable<Child> Items { get; set; }
		public string NextPageLink { get; set; }
		public int Count { get; set; }
	}
}