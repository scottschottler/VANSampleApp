﻿using Microsoft.Azure.Search;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;

namespace VANSampleApp.Models
{
	/// <summary>
	/// Base class for shared fields between azure search model and VAN API
	/// </summary>
	public abstract class PersonBase
	{
		[Key, IsSearchable]
		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public string VanId { get; set; }

		[IsSearchable, Required, MaxLength(20)]
		public string FirstName { get; set; }

		[IsSearchable, Required, MaxLength(25)]
		public string LastName { get; set; }

		[IsFilterable, IsSortable, IsFacetable]
		public DateTime? DateOfBirth { get; set; }

		[IsFilterable, IsFacetable, IsSortable, MaxLength(1)]
		public string Party { get; set; }

		[IsSearchable, IsFilterable, IsFacetable, IsSortable, MaxLength(50)]
		public string Employer { get; set; }

		[IsSearchable, IsFilterable, IsFacetable, IsSortable, MaxLength(50)]
		public string Occupation { get; set; }

		protected void CopyProperties(PersonBase person)
		{
			VanId = person.VanId;
			FirstName = person.FirstName;
			LastName = person.LastName;
			DateOfBirth = person.DateOfBirth;
			Party = person.Party;
			Employer = person.Employer;
			Occupation = person.Occupation;
		}
	}

	/// <summary>
	/// VAN API Model
	/// </summary>
	public class Person : PersonBase
	{
		public Person() { }
		public Person(PersonSearchModel person)
		{
			CopyProperties(person);

			if (!string.IsNullOrEmpty(person.AddressLine1))
			{
				Addresses = new[]
				{
					new Address
					{
						AddressId = person.AddressId,
						AddressLine1 = person.AddressLine1,
						AddressLine2 = person.AddressLine2,
						City = person.City,
						StateOrProvince = person.StateOrProvince,
						ZipOrPostalCode = person.ZipOrPostalCode,
						CountryCode = person.CountryCode
					}
				};
			}

			if (!string.IsNullOrEmpty(person.Email))
			{
				Emails = new[] { new EmailAddress(person.Email) };
			}

			if (!string.IsNullOrEmpty(person.PhoneNumber))
			{
				Phones = new[] { new Phone(person.PhoneNumber) };
			}
		}

		public Address[] Addresses { get; set; }

		public EmailAddress[] Emails { get; set; }

		public Phone[] Phones { get; set; }
	}

	/// <summary>
	/// Azure Search Model (flattened for indexing)
	/// </summary>
	public class PersonSearchModel : PersonBase
	{
		public PersonSearchModel() { }

		public PersonSearchModel(Person person)
		{
			CopyProperties(person);

			Email = person.Emails?.FirstOrDefault()?.Email;
			PhoneNumber = person.Phones?.FirstOrDefault()?.PhoneNumber;

			var address = person.Addresses?.FirstOrDefault();

			if (address != null)
			{
				AddressId = address.AddressId;
				AddressLine1 = address.AddressLine1;
				AddressLine2 = address.AddressLine2;
				City = address.City;
				StateOrProvince = address.StateOrProvince;
				ZipOrPostalCode = address.ZipOrPostalCode;
				CountryCode = address.CountryCode;
			}
		}

		[IsSearchable]
		public string Email { get; set; }

		[IsSearchable]
		public string PhoneNumber { get; set; }

		public int? AddressId { get; set; }

		[IsSearchable]
		public string AddressLine1 { get; set; }

		public string AddressLine2 { get; set; }

		[IsSearchable, IsFilterable, IsFacetable, IsSortable]
		public string City { get; set; }

		[IsSearchable, IsFilterable, IsFacetable, IsSortable]
		public string StateOrProvince { get; set; }

		[IsSearchable, IsFilterable, IsFacetable, IsSortable]
		public string ZipOrPostalCode { get; set; }

		[IsSearchable, IsFilterable, IsFacetable, IsSortable]
		public string CountryCode { get; set; }
	}

	/// <summary>
	/// Response from VAN API people endpoints
	/// </summary>
	public class Match
	{
		public int VanId { get; set; }

		public int HttpCode { get; set; }

		public string Status { get; set; }
	}
}