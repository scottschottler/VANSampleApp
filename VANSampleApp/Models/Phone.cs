﻿using System.ComponentModel.DataAnnotations;

namespace VANSampleApp.Models
{
	public class Phone
	{
		public Phone(string phoneNumber) => PhoneNumber = phoneNumber;

		[Required]
		public string PhoneNumber { get; set; }

		//public string PhoneType { get; set; }

		//public string Ext { get; set; }

		//public bool IsPreferred { get; set; }
	}
}