﻿using System.ComponentModel.DataAnnotations;

namespace VANSampleApp.Models
{
	public enum DBMode { VoterFile = 0, MyCampaign = 1 }

	public class VANRequest
	{
		[EnumDataType(typeof(DBMode), ErrorMessage = "DBMode must be either 0 (VoterFile) or 1 (MyCampaign)")]
		public DBMode DBMode { get; set; }

		// (?i) is for case-insensitivity:
		[RegularExpression("(?i)(GET|POST|PUT|DELETE)", ErrorMessage = "Invalid HTTP Verb")]
		public string Verb { get; set; }

		[Required]
		public string Endpoint { get; set; }

		public string Json { get; set; }
	}
}