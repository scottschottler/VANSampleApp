﻿using Microsoft.Azure.Search;
using Microsoft.Azure.Search.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VANSampleApp.Config;
using VANSampleApp.Extensions;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public class AzureSearchService : ISearchService
	{
		private readonly AzureSearchSettings _settings;
		private readonly SearchServiceClient _client;
		private readonly ILogger<AzureSearchService> _logger;

		public AzureSearchService(IOptions<AzureSearchSettings> options, ILogger<AzureSearchService> logger)
		{
			_logger = logger;
			_settings = options.Value;

			if (_settings.Configured)
				_client = new SearchServiceClient(_settings.ServiceName, new SearchCredentials(_settings.AdminKey));
		}

		public void CreatePeopleIndex()
		{
			if (_settings.Configured)
			{
				try
				{
					DeletePeopleIndex();

					var definition = new Index()
					{
						Name = _settings.PeopleIndexName,
						Fields = FieldBuilder.BuildForType<PersonSearchModel>()
					};

					_client.Indexes.Create(definition);
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, ex.Message);
				}
			}
		}

		public void DeletePeopleIndex()
		{
			if (_client.Indexes.Exists(_settings.PeopleIndexName))
				_client.Indexes.Delete(_settings.PeopleIndexName);
		}

		private void EnsureIndexExists()
		{
			if (!_client.Indexes.Exists(_settings.PeopleIndexName))
				CreatePeopleIndex();
		}

		public async Task<bool> IndexPersonAsync(Person person)
		{
			if (_settings.Configured)
			{
				var searchModel = new PersonSearchModel(person);

				EnsureIndexExists();

				var indexClient = _client.Indexes.GetClient(_settings.PeopleIndexName);

				var documents = new PersonSearchModel[] { searchModel };
				var batch = IndexBatch.Upload(documents);

				try
				{
					var result = await indexClient.Documents.IndexAsync(batch);
					return result.Results[0].Succeeded;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, ex.Message);
				}
			}

			return false;
		}

		public IEnumerable<Person> SearchPeople(string query)
		{
			if (_settings.Configured)
			{
				EnsureIndexExists();

				query = WildcardAllTerms(query);

				var client = CreateSearchIndexClient();

				var results = client.Documents.Search<PersonSearchModel>(query, new SearchParameters
				{
					SearchMode = SearchMode.All,
					Top = 500
				});

				return results.Results.Select(searchResult => new Person(searchResult.Document));
			}

			return null;
		}

		private ISearchIndexClient CreateSearchIndexClient()
		{
			return new SearchIndexClient(_settings.ServiceName, _settings.PeopleIndexName, new SearchCredentials(_settings.QueryKey));
		}
		
		private string WildcardAllTerms(string q) 
			=> string.Join(" ", q.ToTerms().Select(p => $"{p}*"));
	}
}