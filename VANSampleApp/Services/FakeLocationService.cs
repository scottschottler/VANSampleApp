﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	/// <summary>
	/// Fake ILocationService for testing/prototyping without blasting the VAN API and allowing 
	/// people to run the app without having a VAN API key set in appsettings.json or secrets.json
	/// </summary>
	public class FakeLocationService : ILocationService
	{
		public Task<Location> GetLocationAsync(int locationId)
		{
			return Task.FromResult(GenerateFakeData().FirstOrDefault(l => l.LocationId == locationId));
		}

		public Task<PagedResponse<Location>> GetLocationsAsync(string name, int? top, int? skip)
		{
			var locations = GenerateFakeData();

			var result = new PagedResponse<Location>
			{
				Count = locations.Count()
			};

			if (!string.IsNullOrEmpty(name))
			{
				name = name.ToLower();
				locations = locations.Where(location => location.Name.ToLower().Contains(name));
				result.Count = locations.Count();
			}

			if (skip.HasValue)
				locations = locations.Skip(skip.Value);
			
			locations = locations.Take(top ?? 10);

			result.Items = locations;

			return Task.FromResult(result);
		}

		public Task<int> SaveLocationAsync(Location location)
		{
			// fake successful save returning the location ID:
			return Task.FromResult(3);
		}

		public Task<bool> DeleteAsync(int locationId)
		{
			// fake successful save. No actual persistence for fake data
			return Task.FromResult(true);
		}

		private IEnumerable<Location> GenerateFakeData()
		{
			var locations = new List<Location>
			{
				new Location
				{
					Name = "Arlington Drafthouse",
					Address = new Address
					{
						Preview = "2903 Columbia Pike, Arlington, VA 22204",
						GeoLocation = new GeoLocation(38.862603m, -77.087386m)
					}
				},
				new Location
				{
					Name = "6th & I",
					Address = new Address
					{
						Preview = "600 I St NW, Washington, DC 20003",
						GeoLocation = new GeoLocation(38.900632m, -77.020256m)
					}
				},
				new Location
				{
					Name = "Studio Theatre",
					Address = new Address
					{
						Preview = "1501 14th St NW, Washington, DC 20005",
						GeoLocation = new GeoLocation(38.910008m, -77.031551m)
					}
				},
				new Location
				{
					Name = "DC Improv ",
					Address = new Address
					{
						Preview = "1140 Connecticut Ave NW, Washington, DC 20036",
						GeoLocation = new GeoLocation(38.904974m, -77.041200m)
					}
				},
				new Location
				{
					Name = "DC Drafthouse",
					Address = new Address
					{
						Preview = "1100 13th St NW, Washington, DC 20005",
						GeoLocation = new GeoLocation(38.904085m, -77.030089m)
					}
				},
				new Location
				{
					Name = "Black Cat",
					Address = new Address
					{
						Preview = "1811 14th St NW, Washington, DC 20009",
						GeoLocation = new GeoLocation(38.914584m, -77.031538m)
					}
				},
				new Location
				{
					Name = "Kennedy Center",
					Address = new Address
					{
						Preview = "2700 F St NW, Washington, DC 20566",
						GeoLocation = new GeoLocation(38.896151m, -77.055447m)
					}
				},
				new Location
				{
					Name = "Scott & Julie's Apartment",
					Address = new Address
					{
						Preview = "2301 Columbia Pike, Arlington, VA 22204",
						AddressLine1 = "2301 Columbia Pike",
						City = "Arlington",
						StateOrProvince = "VA",
						ZipOrPostalCode = "22204",
						CountryCode = "US",
						GeoLocation = new GeoLocation(38.8639336m, -77.0828594m)
					}
				},
				new Location
				{
					Name = "Scott's old apartment",
					Address = new Address
					{
						Preview = "443 New York Ave NW #314, Washington, DC 20001",
						GeoLocation = new GeoLocation(38.904609m, -77.017988m)
					}
				},
				new Location
				{
					Name = "National Museum of African American History and Culture",
					Address = new Address
					{
						Preview = "1400 Constitution Ave NW, Washington, DC 20560",
						GeoLocation = new GeoLocation(38.891064m, -77.032614m)
					}
				},
				new Location
				{
					Name = "The White House",
					Address = new Address
					{
						Preview = "1600 Pennsylvania Ave NW, Washington, DC 20500",
						GeoLocation = new GeoLocation(38.897663m, -77.036574m)
					}
				},
			};
			
			for (int i = 0; i < locations.Count; i++)
				locations[i].LocationId = i + 1;

			return locations;
		}
	}
}