﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	/// <summary>
	/// Fake IPeopleService for testing/prototyping without blasting the VAN API and allowing 
	/// people to run the app without having a VAN API key set in appsettings.json or secrets.json
	/// </summary>
	public class FakePeopleService : IPeopleService
	{
		public Task<Person> GetPersonAsync(int vanId)
		{
			return Task.FromResult(GenerateFakeData().FirstOrDefault(p => p.VanId == vanId.ToString()));
		}

		public IEnumerable<Person> GetPeople()
		{
			return GenerateFakeData();
		}

		public IEnumerable<Person> Search(string query)
		{
			query = query.ToLower().Trim();

			return GenerateFakeData().Where(p =>
			{
				return (
					p.FirstName.ToLower().Contains(query) ||
					p.LastName.ToLower().Contains(query) ||
					p.VanId.Contains(query) 
				);
			});
		}

		public Task<Match> SavePersonAsync(Person person)
		{
			// fake successful save:
			return Task.FromResult(new Match
			{
				HttpCode = (int)HttpStatusCode.Created,
				Status = "Stored",
				VanId = 3333333
			});
		}

		private IEnumerable<Person> GenerateFakeData()
		{
			var fakePhone = new Phone("123-456-7890");

			var fakeAddress = new Address
			{
				AddressLine1 = "1234 Fake Lane",
				City = "Fakeville",
				StateOrProvince = "Virginia",
				ZipOrPostalCode = "12345",
				CountryCode = "US"
			};

			return new List<Person>
			{
				new Person
				{
					VanId = "101109766",
					FirstName = "Scott",
					LastName = "Schottler",
					Employer="ELITE Software Consulting",
					Occupation = "Rockstar",
					DateOfBirth = DateTime.Parse("3/8/1986"),
					Phones = new [] { new Phone("703-870-9181") },
					Emails = new [] { new EmailAddress("scottschottler@gmail.com") },
					Addresses = new []
					{
						new Address
						{
							AddressLine1 = "2301 Columbia Pike",
							AddressLine2 = "Apt 433",
							City = "Arlington",
							StateOrProvince = "VA",
							ZipOrPostalCode = "22204",
							CountryCode = "US"
						}
					}
				},
				new Person
				{
					VanId = "101109767",
					FirstName = "Barack",
					LastName = "Obama",
					Occupation = "Politican",
					DateOfBirth = DateTime.Parse("8/4/1961"),
					Phones = new [] { fakePhone },
					Emails = new [] { new EmailAddress("barackobama@gmail.com") },
					Addresses = new [] { fakeAddress }
				},
				new Person
				{
					VanId = "101109765",
					FirstName = "Nathan",
					LastName = "Fielder",
					Occupation = "Consultant",
					DateOfBirth = DateTime.Parse("5/12/1983"),
					Phones = new [] { fakePhone },
					Emails = new [] { new EmailAddress("nathanfielder@nathanfielder.com") },
					Addresses = new [] { fakeAddress }
				},
				new Person
				{
					VanId = "101109764",
					FirstName = "Marc",
					LastName = "Maron",
					Occupation = "Comedian",
					DateOfBirth = DateTime.Parse("9/27/1963"),
					Phones = new [] { fakePhone },
					Emails = new [] { new EmailAddress("marcmaron@wtfpod.com") },
					Addresses = new [] { fakeAddress }
				},
				new Person
				{
					VanId = "101109763",
					FirstName = "Stormy",
					LastName = "Daniels",
					Occupation = "Pornstar",
					DateOfBirth = DateTime.Parse("9/27/1963"),
					Phones = new [] { fakePhone },
					Emails = new [] { new EmailAddress("stormydaniels@gmail.com") },
					Addresses = new [] { fakeAddress }
				}
			};
		}
	}
}