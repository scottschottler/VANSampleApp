﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public interface ICacheService
	{
		IEnumerable<Person> GetPeople();
		Task<bool> SetPeopleAsync(IEnumerable<Person> people);
	}
}