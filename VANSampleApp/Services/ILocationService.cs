﻿using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public interface ILocationService
	{
		Task<Location> GetLocationAsync(int locationId);
		Task<PagedResponse<Location>> GetLocationsAsync(string name, int? top, int? skip);
		Task<int> SaveLocationAsync(Location location);
		Task<bool> DeleteAsync(int locationId);
	}
}