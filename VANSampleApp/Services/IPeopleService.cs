﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public interface IPeopleService
	{
		Task<Person> GetPersonAsync(int vanId);
		IEnumerable<Person> GetPeople();
		IEnumerable<Person> Search(string query);
		Task<Match> SavePersonAsync(Person person);
	}
}