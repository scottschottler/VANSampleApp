﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public interface ISearchService
	{
		void CreatePeopleIndex();
		void DeletePeopleIndex();
		IEnumerable<Person> SearchPeople(string query);
		Task<bool> IndexPersonAsync(Person person);
	}
}