﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
    public interface IVANClientService
    {
		Task<T> GetAsync<T>(string endpoint);
		Task<T> PostAsync<T>(string endpoint, object data);
		Task<HttpStatusCode> DeleteAsync(string endpoint);
		Task<HttpResponseMessage> RequestAsync(VANRequest request);
    }
}