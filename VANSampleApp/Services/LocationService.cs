﻿using System.Net;
using System.Threading.Tasks;
using System.Web;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public class LocationService : ILocationService
	{
		private const string EndPoint = "locations";

		private readonly IVANClientService _vanClient;

		public LocationService(IVANClientService vanClient) => _vanClient = vanClient;

		public async Task<Location> GetLocationAsync(int locationId)
		{
			var location = await _vanClient.GetAsync<Location>($"{EndPoint}/{locationId}");
			return location;
		}

		public async Task<PagedResponse<Location>> GetLocationsAsync(string name, int? top, int? skip)
		{
			var endpoint = $"{EndPoint}?$top={top ?? 10}";
			
			if (skip.HasValue)
				endpoint += $"&$skip={skip}";

			if (!string.IsNullOrEmpty(name))
				endpoint += $"&name={HttpUtility.UrlEncode(name)}";

			var locations = await _vanClient.GetAsync<PagedResponse<Location>>(endpoint);
			return locations;
		}

		public async Task<int> SaveLocationAsync(Location location)
		{
			var locationId = await _vanClient.PostAsync<int>(EndPoint, location);
			return locationId;
		}

		public async Task<bool> DeleteAsync(int locationId)
		{
			string endpoint = $"{EndPoint}/{locationId}";
			var statusCode = await _vanClient.DeleteAsync(endpoint);
			return statusCode == HttpStatusCode.NoContent;
		}
	}
}