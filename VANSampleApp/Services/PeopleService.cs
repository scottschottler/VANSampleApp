﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public class PeopleService : IPeopleService
	{
		private const string EndPoint = "people";

		private readonly IVANClientService _vanClient;
		private readonly ISearchService _searchService;
		private readonly ICacheService _cacheService;

		public PeopleService(IVANClientService vanClient, ISearchService searchService, ICacheService cacheService)
		{
			_vanClient = vanClient;
			_searchService = searchService;
			_cacheService = cacheService;
		}

		public IEnumerable<Person> Search(string query)
		{
			return _searchService.SearchPeople(query);
		}

		public IEnumerable<Person> GetPeople()
		{
			var result = _cacheService.GetPeople();

			if (result == null)
			{
				result = _searchService.SearchPeople(string.Empty);

				if (result?.Count() > 0)
					_cacheService.SetPeopleAsync(result);
			}

			return result;
		}

		public async Task<Person> GetPersonAsync(int vanId)
		{
			string endpoint = $"{EndPoint}/{vanId}";
			var person = await _vanClient.GetAsync<Person>(endpoint);
			return person;
		}

		public async Task<Match> SavePersonAsync(Person person)
		{
			string endpoint = $"{EndPoint}/findOrCreate";
			var match = await _vanClient.PostAsync<Match>(endpoint, person);

			if (match.Status.ToLower() == "unmatchedstored")
			{
				person.VanId = match.VanId.ToString();

				await Task.WhenAll(
					_searchService.IndexPersonAsync(person),
					// clear cache so next GetPeople() call refreshes data:
					_cacheService.SetPeopleAsync(null));
			}

			return match;
		}
	}
}