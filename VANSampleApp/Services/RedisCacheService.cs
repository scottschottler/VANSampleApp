﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VANSampleApp.Config;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public class RedisCacheService : ICacheService
	{
		private const string PeopleKey = "people";

		private readonly RedisCacheSettings _redisCacheSettings;
		private readonly Lazy<ConnectionMultiplexer> _connection;

		public RedisCacheService(IOptions<RedisCacheSettings> redisCacheSettings)
		{
			_redisCacheSettings = redisCacheSettings.Value;

			_connection = new Lazy<ConnectionMultiplexer>(() =>
			{
				string cacheConnectionString = _redisCacheSettings.ConnectionString;
				return ConnectionMultiplexer.Connect(cacheConnectionString);
			});
		}

		public IEnumerable<Person> GetPeople()
		{
			if (_redisCacheSettings.Configured)
			{
				IDatabase cache = _connection.Value.GetDatabase();
				string serializedPeople = cache.StringGet(PeopleKey);

				if (!string.IsNullOrEmpty(serializedPeople))
					return JsonConvert.DeserializeObject<IEnumerable<Person>>(serializedPeople);
			}

			return null;
		}

		public async Task<bool> SetPeopleAsync(IEnumerable<Person> people)
		{
			if (!_redisCacheSettings.Configured)
				return false;

			IDatabase cache = _connection.Value.GetDatabase();
			var result = await cache.StringSetAsync(PeopleKey, JsonConvert.SerializeObject(people));
			return result;
		}
	}
}