﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using VANSampleApp.Config;
using VANSampleApp.Models;

namespace VANSampleApp.Services
{
	public class VANClientService : IVANClientService
	{
		private readonly VANSettings _vanSettings;
		private readonly HttpClient _client;

		public VANClientService(IOptions<VANSettings> vanSettings)
		{
			_vanSettings = vanSettings.Value;

			if (!_vanSettings.Configured)
				throw new ConfigurationErrorsException($"{nameof(VANSettings)} not configured. Set your API Key and application name in appsettings.json or secrets.json");

			_client = new HttpClient
			{
				BaseAddress = new Uri(_vanSettings.ServiceURL)
			};

			_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			SetAuthorization();
		}

		private void SetAuthorization(DBMode dbMode = DBMode.MyCampaign)
		{
			var username = _vanSettings.ApplicationName;
			var password = $"{_vanSettings.APIKey}|{dbMode}";
			string token = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{username}:{password}"));

			_client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", token);
		}

		public async Task<T> GetAsync<T>(string endpoint)
		{
			var response = await _client.GetAsync(endpoint);
			CheckErrors(response);
			var result = await response.Content.ReadAsAsync<T>();

			return result;
		}

		public async Task<T> PostAsync<T>(string endpoint, object data)
		{
			//var response = await _client.PostAsJsonAsync(endpoint, data);
			var json = JsonConvert.SerializeObject(data);
			var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

			var response = await _client.PostAsync(endpoint, httpContent);
			CheckErrors(response);
			var result = await response.Content.ReadAsAsync<T>();

			return result;
		}

		public async Task<HttpStatusCode> DeleteAsync(string endpoint)
		{
			var response = await _client.DeleteAsync(endpoint);
			CheckErrors(response);

			return response.StatusCode;
		}

		public async Task<HttpResponseMessage> RequestAsync(VANRequest vanRequest)
		{
			SetAuthorization(vanRequest.DBMode);
			var endpoint = vanRequest.Endpoint.TrimStart('/');

			var method = new HttpMethod(vanRequest.Verb);

			var request = new HttpRequestMessage(method, vanRequest.Endpoint);

			if (method == HttpMethod.Post || method == HttpMethod.Put)
				request.Content = new StringContent(vanRequest.Json, Encoding.UTF8, "application/json");

			var response = await _client.SendAsync(request);
			return response;
		}

		private void CheckErrors(HttpResponseMessage response)
		{
			// https://developers.ngpvan.com/van-api#van-errors
			if (response.StatusCode >= HttpStatusCode.BadRequest)
			{
				var error = response.Content.ReadAsAsync<ErrorResponse>().Result;
				throw new VANException(error.Errors);
			}
		}
	}
}