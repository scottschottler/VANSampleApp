using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VANSampleApp.Config;
using VANSampleApp.Filters;
using VANSampleApp.Services;

namespace VANSampleApp
{
	public class Startup
	{
		public Startup(IConfiguration configuration) => Configuration = configuration;

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services
				.AddMvc(config => config.Filters.Add(typeof(VANExceptionFilter)))
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			// In production, the React files will be served from this directory
			services.AddSpaStaticFiles(configuration => configuration.RootPath = "ClientApp/build");

			services
				.Configure<VANSettings>(Configuration.GetSection("VANSettings"))
				.Configure<AzureSearchSettings>(Configuration.GetSection("AzureSearchSettings"))
				.Configure<RedisCacheSettings>(Configuration.GetSection("RedisCacheSettings"));

			var vanSettings = new VANSettings();
			Configuration.Bind("VANSettings", vanSettings);

			services.AddScoped<IVANClientService, VANClientService>();

			if (vanSettings.Configured)
			{
				services
					.AddScoped<ILocationService, LocationService>()
					.AddScoped<IPeopleService, PeopleService>()
					// only used by PeopleService. App will still run if they're not configured:
					.AddScoped<ICacheService, RedisCacheService>()
					.AddScoped<ISearchService, AzureSearchService>();
			}
			else
			{
				// if VAN API not configured, inject the fake services so the app still runs:
				services
					.AddScoped<ILocationService, FakeLocationService>()
					.AddScoped<IPeopleService, FakePeopleService>();
			}
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				app.UseExceptionHandler("/Error");
				app.UseHsts();
			}

			app.UseHttpsRedirection();
			app.UseStaticFiles();
			app.UseSpaStaticFiles();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller}/{action=Index}/{id?}");
			});

			app.UseSpa(spa =>
			{
				spa.Options.SourcePath = "ClientApp";

				if (env.IsDevelopment())
				{
					// 1st option: automatically run "npm start" script on startup
					// advantage: you don't have to run "npm start" manually from ClientApp folder
					// disadvantage: whenever you have to modify C# code and rerun the app, you will have to wait for the CRA server to start (takes an extra 7-8 seconds on my machine):
					spa.UseReactDevelopmentServer(npmScript: "start");

					// https://docs.microsoft.com/en-us/aspnet/core/client-side/spa/react?view=aspnetcore-2.1&tabs=visual-studio#run-the-cra-server-independently
					// 2nd option: manually run "npm start" script from ClientApp folder
					// advantage: you don't have to wait for the CRA server to start every time you rerun the app after changing C# code 
					// disadvantage: you have a separate manual step to running the app initially
					// spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
				}
			});
		}
	}
}